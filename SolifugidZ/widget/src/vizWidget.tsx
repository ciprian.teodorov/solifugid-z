//
// Code to draw a simple directed graph as an SVG element. This element is wrapped in react-svg-pan-zoom for
// more user-friendlt viewing.
//
// This gets compiled into dist/vizWidget.js which is used by VizGraph.lean
// The compiled version .js file is put into source control so you don't have to
// build it yourself when using this package.
//

import React, {useState} from 'react';
import {useWindowSize} from '@react-hook/window-size';

import {Vector2} from 'ts-vector-math';
import {UncontrolledReactSVGPanZoom, TOOL_NONE, fitSelection, zoomOnViewerCenter, fitToViewer, Tool, Value} from 'react-svg-pan-zoom';

// not in typescript exports of react-svg-pan-zoom...
const INITIAL_VALUE={};

interface EdgeData {
  dest : number;
  label : string;
}
interface VertexData {
  index : number;
  label : string;
  outgoing : EdgeData [];
}

interface MarkInfo {
  markLabel : string;
  nodes : number[];
}
interface Graph {
  vertices : VertexData[];
  startNodes : number[];
  endNodes : number[];
  markedNodes : MarkInfo[];
}

interface VizVertex {
  index : number;
  highlighted : boolean;
  position : Vector2;
}

interface VizConfig {
  nodeSize : number;
  nodeSpacing : number;
  nextPathId : number;
  markedSetIndex : number;
}

// look for start nodes. Usually this will be the elements of "startNodes" but if there aren't any,
// we look for nodes with no incoming edges. Failing that, we just pick a node.
function findStartNodes (g : Graph) : number[] {
  if (g.startNodes.length > 0) {
    return g.startNodes;
  }

  // look for node with no predecessor - we gotta look at all nodes!
  // If we have a lotta nodes then a better method should be used, but most graphs
  // we visualize are pretty small (<100 nodes) so it's not too bad.
  const hasPredecessor = function (vix : number) : boolean {
    return g.vertices.some(v => v.outgoing.some(e => e.dest === vix));
  };

  const nopreds = g.vertices.filter(v => !hasPredecessor(v.index));
  if (nopreds.length > 0) {
    return nopreds.map(v => v.index);
  }

  // no start nodes and no nodes with 0 predecessors. Just pick a few nodes as a fallback.
  const fallbackCount = g.vertices.length / 10;
  return g.vertices.slice(0,fallbackCount).map(v => v.index);
}

function findSuccessors(curNodes: number[], remainingNodes: number[], g: Graph) : number[] {
  let accumulator: number[] = [];

  curNodes.forEach(n => {
    let vData = g.vertices.find(x => x.index === n);
    if (vData) {
      vData.outgoing.forEach(e => {
        // Before adding a successor we first check that it isn't
        // already recorded in the accumulator to avoid duplicates
        if (!accumulator.some(i => e.dest === i) && remainingNodes.some(i => e.dest === i)) {
          accumulator.push(e.dest);
        }
      });
    }
  });

  return accumulator;
}

// Given graph data, sort vertices (by index) into groups. Each group is made up of successor nodes of the previous
// group. This is used to determine vertex layout.
function groupLevels(g : Graph) : number[][] {
  // we track which nodes still need to be put in groups
  let remainingNodes = g.vertices.map(v => v.index);

  // as we generate groups of nodes we need to remove them from the array that records remaining nodes
  const contains = function (l : number[], n : number) { return l.some(x => x === n);};
  const filterOut = function (l : number[], filterOut : number[]) { return l.filter(x => !contains(filterOut,x))};

  // get the initial group of nodes
  const level0 = findStartNodes(g);
  remainingNodes = filterOut(remainingNodes, level0);

  let levels = [level0];
  // go until no nodes left to process
  while (remainingNodes.length > 0) {
    const succNodes = findSuccessors(levels[levels.length-1], remainingNodes, g);
    // if there are no successor nodes found then whatever is left cannot be reached,
    // so just dump remaining nodes onto a level and quit
    if (succNodes.length == 0) {
      levels.push(remainingNodes);
      break;
    }
    // otherwise process these nodes
    levels.push(succNodes);
    remainingNodes = filterOut(remainingNodes, succNodes);
  }

  return levels;
}

function placeVertices (g : Graph, c : VizConfig) : VizVertex[] {
  const groups = groupLevels(g);
  const markInfo = g.markedNodes[c.markedSetIndex];
  var markedNodes : number[] = [];
  if (typeof markInfo !== 'undefined') { markedNodes = markInfo.nodes};
  const viz = groups.flatMap((group,level) => {
    return group.map((n,ix) => {
      return {
        index : n,
        highlighted : markedNodes.find(x => n === x) !== undefined,
        position : new Vector2([10 + ix * c.nodeSpacing, 10 + level * c.nodeSpacing])
      };
    });
  });
  return viz;
}

interface GraphBounds {
  minX : number,
  maxX : number,
  minY : number,
  maxY : number
}

function vertexBounds(vs : VizVertex[], cfg : VizConfig) : GraphBounds {
  if (vs.length === 0) {
    return {minX:0, maxX:0, minY:0, maxY:0 };
  }
  const pos0 = vs[0].position;
  const sz = cfg.nodeSize + 4;
  let accumulator : GraphBounds = {minX:pos0.x, maxX:pos0.y, minY:pos0.x, maxY:pos0.y};
  vs.forEach(v => {
    const x = v.position.x;
    const y = v.position.y;
    accumulator.minX = Math.min(x-sz, accumulator.minX);
    accumulator.maxX = Math.max(x+sz, accumulator.maxX);
    accumulator.minY = Math.min(y-sz, accumulator.minY);
    accumulator.maxY = Math.max(y+sz, accumulator.maxY);
  });
  return accumulator;
}

function genEdgePath (start : Vector2, end : Vector2, label : string, cfg : VizConfig) : JSX.Element {
  const rotateVec2 = (v : Vector2, r : number) => { return new Vector2([v.x * Math.cos(r) + v.y * Math.sin(r), -v.x * Math.sin(r) + v.y * Math.cos(r)]); };

  const edgeVector = end.copy().subtract(start);
  var dirForward = edgeVector.copy().normalize();
  var dirBack = dirForward.copy().negate();

  // instead of drawing a path straight between the center of two nodes, we shift it
  // so that the edge curves slightly clockwise. Curve
  let edgeDeflect = 0.3; // in radians

  // for a self-loop we make the deflections larger and opposite so it generates a loop on the node
  if (edgeVector.length() < cfg.nodeSize)
  {
    dirForward = new Vector2([1,0]);
    dirBack = new Vector2([1,0]);
    edgeDeflect = -0.5;
  }

  // We need the paths to start/stop on node edges, but these coords are from
  // node center. So we need to move endpoints closer together by the nodeSize.
  const d1 = rotateVec2(dirForward, edgeDeflect).scale(cfg.nodeSize);
  const d2 = rotateVec2(dirBack, -edgeDeflect).scale(cfg.nodeSize);
  const p1 = start.copy().add(d1);
  const p2 = end.copy().add(d2);

  // generate a bezier slightly curved clockwise or widdershins
  let c1 = p1.copy().add(d1);
  let c2 = p2.copy().add(d2);

  // arrow hooks are N radians off of the main arrow line
  const hookD1 = rotateVec2(d2, 0.4).normalize().scale(4);
  const hookD2 = rotateVec2(d2, -0.4).normalize().scale(4);
  const h1 = p2.copy().add(hookD1);
  const h2 = p2.copy().add(hookD2);

  // place label about 1/10 of the way to the destination
  const labelPos = p1.copy().add(edgeVector.copy().scale(0.1));

  // each path gets a unique ID so we can align text to the path
  const pathId = "path_" + cfg.nextPathId;
  cfg.nextPathId = cfg.nextPathId+1;

  const pathString = `M ${p1.x} ${p1.y} C ${c1.x} ${c1.y}, ${c2.x} ${c2.y}, ${p2.x} ${p2.y} L ${h1.x} ${h1.y} M ${p2.x} ${p2.y} L ${h2.x} ${h2.y}`;
  return <g>
    <path id={pathId} d={pathString} stroke="black" fill="transparent" strokeWidth="1"/>
    <text>
      <textPath href={"#"+pathId} style={{fontSize: 8}} x={labelPos.x} y={labelPos.y} text-anchor="left" startOffset="1%" baseline-shift="10%">{label}</textPath>
    </text>
    </g>;
}

function genEdge(d : VizVertex, e : EdgeData, viz : VizVertex[], cfg : VizConfig) : JSX.Element {
  const destVertex = viz.find(v => v.index === e.dest);
  if (typeof destVertex == 'undefined') {
    return <path/>;
  }
  else {
    const edgeElement = genEdgePath(d.position, destVertex.position, e.label, cfg);
    return edgeElement;
  }
}

function drawVertex (d : VizVertex, vs : VertexData[], viz : VizVertex[], cfg : VizConfig) : JSX.Element {
  const strokeColor = d.highlighted ? "red" : "black";
  const vertexDraw = <circle cx={d.position.x} cy={d.position.y} r={cfg.nodeSize} fill="none" stroke={strokeColor} />;
  const vInfo = vs.find(x => x.index === d.index);
  if (typeof vInfo == 'undefined') {
    return vertexDraw;
  }
  else
  {
    const label = <text style={{fontSize: 8}} x={d.position.x+8} y={d.position.y} text-anchor="middle"> {vInfo.label} </text>;
    const edges = vInfo.outgoing.map(e => genEdge(d, e, viz, cfg));
    return <g>
        {vertexDraw}
        {label}
        {...edges}
      </g>;
  }
}

export default function (props: any) {
  const g : Graph = props || [];
  const gr = groupLevels(g);

  const [markIndex, setMarkIndex] = useState(0);
  const newMarkIndex = (m : number) => { setMarkIndex(Math.max(0,Math.min(m, g.markedNodes.length-1)))}
  const prevMark = () => { newMarkIndex(markIndex-1) }
  const nextMark = () => { newMarkIndex(markIndex+1) }

  const config : VizConfig = {
    nodeSize : 30,
    nodeSpacing : 110,
    nextPathId : 0,
    markedSetIndex : markIndex
  };
  const viz = placeVertices(g, config);
  const bounds = vertexBounds(viz, config);

  const Viewer = React.useRef<UncontrolledReactSVGPanZoom>(null);

  React.useEffect(() => {
    if (Viewer.current) {
      Viewer.current.fitToViewer();
    }
  }, []);

  // we try to autosize view area to the infoview pane. Note we need to subtract about 50px
  // from width since Lean infoview adds a buffer/gutter on the left size.
  const [width, height] = useWindowSize({initialWidth: 400, initialHeight: 400});

  const viewBoxStr = `${bounds.minX} ${bounds.minY} ${bounds.maxX-bounds.minX} ${bounds.maxY-bounds.minY}`;
  var markLabel = "";
  if (g.markedNodes.length > 0) {
    markLabel = (markIndex+1) + "/" + (g.markedNodes.length) + " marked: " + g.markedNodes[markIndex].markLabel;
  }

  return <div>
      <div hidden={true}>Sequence: {JSON.stringify(gr)}</div>
      <div>
        <button onClick={prevMark}>&lt;</button>
        <button onClick={nextMark}>&gt;</button>
        {markLabel}
      </div>
      <UncontrolledReactSVGPanZoom
        ref={Viewer}
        width={width-50} height={height}
        background="#919274"
        defaultTool="pan"
        >
        <svg viewBox={viewBoxStr} xmlns="http://www.w3.org/2000/svg">
          {viz.map(d => drawVertex(d, g.vertices, viz, config))}
        </svg>
      </UncontrolledReactSVGPanZoom>
    </div>
}

