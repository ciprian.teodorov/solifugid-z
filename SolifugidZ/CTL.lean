--
-- CTL property checking
--

import Lean

import SolifugidZ.Basic
import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.GraphUtilities
import SolifugidZ.FSM
import Examples.TrafficLight

namespace CTL

open Basic
open APBits

-- CTL logic
-- CTL has two expression types, state formulae and path formulae.

inductive CTLType where
    | Path
    | State
    deriving BEq, Repr

inductive CTLFormula (A : Type) : CTLType → Type where
    | True : CTLFormula A .State 
    | AP : A → CTLFormula A .State
    | And : CTLFormula A .State → CTLFormula A .State → CTLFormula A .State
    | Or : CTLFormula A .State → CTLFormula A .State → CTLFormula A .State
    | Implies : CTLFormula A .State → CTLFormula A .State → CTLFormula A .State
    | Not : CTLFormula A .State → CTLFormula A .State
    | ExistsPath : CTLFormula A .Path → CTLFormula A .State
    | ForallPaths : CTLFormula A .Path → CTLFormula A .State
    | Next : CTLFormula A .State → CTLFormula A .Path
    | Always : CTLFormula A .State → CTLFormula A .Path
    | Eventually : CTLFormula A .State → CTLFormula A .Path
    | Until : CTLFormula A .State → CTLFormula A .State → CTLFormula A .Path
    deriving BEq, Repr

def ctlString [ToString A] : CTLFormula A t → String
| .True => "True"
| .AP a => "<+" ++ toString a ++ "+>"
| .And a b => ctlString a ++ "∧" ++ ctlString b
| .Or a b => ctlString a ++ "∨" ++ ctlString b
| .Implies a b => ctlString a ++ "→" ++ ctlString b
| .Not a => "¬" ++ ctlString a
| .ExistsPath p => "∃" ++ ctlString p
| .ForallPaths p => "∀" ++ ctlString p
| .Next p => "◯" ++ ctlString p
| .Always p => "□" ++ ctlString p
| .Eventually p => "◇" ++ ctlString p
| .Until a b => ctlString a ++ "⋃" ++ ctlString b

instance [ToString A] : ToString (CTLFormula A t) where
    toString := ctlString

-- parsing is similar to LTL but we need separate categories for path and state expressions
declare_syntax_cat ctlpath
declare_syntax_cat ctlstate

syntax "[ctl" ctlstate "]" : term
syntax "[ctlpath" ctlpath "]" : term

syntax " <+ " term " +> " : ctlstate
syntax ctlstate "∧" ctlstate : ctlstate
syntax ctlstate "∨" ctlstate : ctlstate
syntax "¬" ctlstate : ctlstate
syntax ctlstate "→" ctlstate : ctlstate
syntax "∀" ctlpath : ctlstate
syntax "∃" ctlpath : ctlstate

syntax "□" ctlstate : ctlpath
syntax "◇" ctlstate : ctlpath
syntax "◯" ctlstate : ctlpath
syntax ctlstate "⋃" ctlstate : ctlpath

syntax "(" ctlpath ")" : ctlpath
syntax "(" ctlstate ")" : ctlstate

macro_rules
  | `([ctl <+ $t:term +> ]) => `(CTLFormula.AP $t:term)
  | `([ctl ¬ $a:ctlstate ]) => `(CTLFormula.Not [ctl $a ])
  | `([ctl $a ∧ $b ])       => `(CTLFormula.And [ctl $a ] [ctl $b ])
  | `([ctl $a ∨ $b ])       => `(CTLFormula.Or [ctl $a ] [ctl $b ])
  | `([ctl $a → $b])        => `(CTLFormula.Implies [ctl $a ] [ctl $b ])
  | `([ctl ∀ $a:ctlpath])   => `(CTLFormula.ForallPaths [ctlpath $a])
  | `([ctl ∃ $a:ctlpath])   => `(CTLFormula.ExistsPath [ctlpath $a])
  | `([ctl ( $a ) ])        => `([ctl $a ])

  | `([ctlpath □ $a:ctlstate ]) => `(CTLFormula.Always [ctl $a ])
  | `([ctlpath ◇ $a:ctlstate ]) => `(CTLFormula.Eventually [ctl $a ])
  | `([ctlpath ◯ $a:ctlstate ]) => `(CTLFormula.Next [ctl $a ])
  | `([ctlpath $a:ctlstate ⋃ $b:ctlstate ]) => `(CTLFormula.Until [ctl $a ] [ctl $b ])

  | `([ctlpath ( $a ) ]) => `([ctlpath $a ])

--#check [ctl ∀□∃◇<+3+>]

-- CTL in existential normal form (ENF)
-- This is equivalent in expressiveness to regular CTL but limits the number of different operators/combinations
-- we need to examine.
inductive CTLENF (A : Type) : CTLType → Type where
    | True : CTLENF A .State 
    | AP : A → CTLENF A .State
    | And : CTLENF A .State → CTLENF A .State → CTLENF A .State
    | Not : CTLENF A .State → CTLENF A .State
    | Exists : CTLENF A .Path → CTLENF A .State
    | Next : CTLENF A .State → CTLENF A .Path
    | Always : CTLENF A .State → CTLENF A .Path
    | Until : CTLENF A .State → CTLENF A .State → CTLENF A .Path
    deriving BEq, Repr

-- convert a CTL formula into ENF
def fromCTLtoENF {A : Type} {V : CTLType} : CTLFormula A V → CTLENF A V
    -- note we use (fromCTLtoENF (.Not a)) when possible so that we can eliminate double negations during the conversions
    | .True => .True
    | .AP a => .AP a
    | .And a b => .And (fromCTLtoENF a) (fromCTLtoENF b)
    -- a ∨ b ↔ ¬(¬a ∧ ¬b)
    | .Or a b => 
        let a' := match fromCTLtoENF a with
                  | .Not x => x
                  | z => .Not z
        let b' := match fromCTLtoENF b with
                  | .Not x => x
                  | z => .Not z
        .Not <| .And a' b'
    -- (a → b) ↔ (¬a ∨ b) ↔ ¬(a ∧ ¬ b)
    | .Implies a b => .Not <| .And (fromCTLtoENF a) (.Not <| fromCTLtoENF b)
    | .Not a => match fromCTLtoENF a with
                | .Not x => x
                | z => .Not z
    | .ExistsPath p => .Exists (fromCTLtoENF p)
    | .ForallPaths p => match p with
                        -- ∀◯a ↔ ¬∃◯¬a
                        | .Next a => .Not <| .Exists <| .Next <| fromCTLtoENF (.Not a)
                        -- ∀□a ↔ ¬∃◇¬a ↔ ¬∃(true ⋃ ¬a)
                        | .Always a => .Not <| .Exists <| .Until .True (fromCTLtoENF (.Not a))
                        -- ∀◇a ↔ ¬∃□¬a
                        | .Eventually a => .Not <| .Exists <| .Always <| fromCTLtoENF (.Not a)
                        -- ∀(a ⋃ b) ↔ ¬∃(¬b ⋃ (¬a ∧ ¬b)) ∧ ¬∃□¬b
                        | .Until a b =>
                            let a' := fromCTLtoENF (.Not a)
                            let b' := fromCTLtoENF (.Not b)
                            .And
                                (.Not <| .Exists <| .Until b' (.And a' b'))
                                (.Not <| .Exists <| .Always <| b')
    | .Next a => .Next (fromCTLtoENF a)
    | .Always a => .Always (fromCTLtoENF a)
    | .Eventually a => .Until .True (fromCTLtoENF a)
    | .Until a b => .Until (fromCTLtoENF a) (fromCTLtoENF b)


--#eval fromCTLtoENF <| [ctl ∀<+3+>⋃<+4+>]

-- Holds various subformulae and a value associated with each subformulae. When
-- traversing the CTL expression we generate satisfaction sets and put them in here for
-- use by parent subformulae.
structure CTLSubFormulae (A : Type) (V : Type) where
   (stateF : List (CTLENF A .State × V))
   (pathF : List (CTLENF A .Path × V))
   deriving BEq, Repr

-- lookup subformula
def lookupSubformula {C : CTLType} {A V : Type} [BEq A] (c : CTLENF A C) (sf : CTLSubFormulae A V) : Option V :=
    match C with
    | .State => List.lookup c sf.stateF
    | .Path => List.lookup c sf.pathF

-- add a subformula with a given value
def addSubformula  {C : CTLType} {A V : Type} [BEq A] (c : CTLENF A C) (val : V) (sf : CTLSubFormulae A V) : CTLSubFormulae A V :=
    match C with
    | .State => {sf with stateF := ⟨c,val⟩ :: sf.stateF}
    | .Path => {sf with pathF := ⟨c,val⟩ :: sf.pathF}



-- Given some set of vertices of a transition system, finds all vertices which have any element of that set
-- as a successor.
def satExistsNext {VI VL EL : Type} [BEq VI] (ts : FSM VI VL EL) (subSat : List VI) : List VI :=
    let allVerts := List.map ts.vertexFor (listRangeFin ts.vertexCount)
    allVerts.filter <| fun v =>
        let successors := (ts.edgesFor v).el.map (fun ⟨l,_⟩ => l)
        List.any successors subSat.contains

-- given some set of vertices of a transition system, finds all vertices for which that subset is always true.
-- This translate into finding cycles within the subset.
def satExistsAlways {VI VL EL : Type} [BEq VI] (ts : FSM VI VL EL) (subSat : List VI) : List VI :=
    let iterSat := subSat.filter <| fun v =>
        let successors := (ts.edgesFor v).el.map (fun ⟨l,_⟩ => l)
        List.any successors subSat.contains
    -- terminate when we've not removed any more vertices
    if List.length iterSat < List.length subSat
    then satExistsAlways ts iterSat
    else iterSat
  termination_by satExistsAlways _ ss => List.length ss

-- finds the set of vertices for a and b, finds <a ⋃ b> which is all vertices that have a path of states
-- containing <a> that eventually lead to a state containing <b>.
def satExistsUntil {VI VL EL : Type} [BEq VI] (ts : FSM VI VL EL) (untilA : List VI) (untilB : List VI) : List VI :=
    -- any vertices in untilA that transition into untilB are allowed
    let ⟨toAdd, remaining⟩ := untilA.partition <| fun v =>
        let successors := (ts.edgesFor v).el.map (fun ⟨l,_⟩ => l)
        List.any successors untilB.contains
    -- keep repeating until no more vertices are added
    if List.length remaining < List.length untilA
    then satExistsUntil ts remaining (List.eraseDups <| toAdd ++ untilB)
    else untilB
    termination_by satExistsUntil _ uA _ => List.length uA



-- to process a CTL formula we recursively process all the subformulae. There are two mutually recursive functions, one
-- for path formulae and one for state formulae

mutual

-- Process a path formula. Since we're in ENF all paths are existential.
def genSubFormulaePath {A VI VL EL : Type} [BEq A] [BEq VI] (p : CTLENF A .Path) (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) : StateM (CTLSubFormulae A (List VI)) (List VI) := do
    let sf : CTLSubFormulae A (List VI) ← get
    match lookupSubformula p sf with
    | Option.some v => pure v
    | Option.none => do
        let storeAndReturn : List VI → StateM (CTLSubFormulae A (List VI)) (List VI) :=
            fun v => do
                modifyGet (fun sf => ⟨(), addSubformula p v sf⟩)
                pure v
        match p with
        | .Next a => do
            let av ← genSubFormulae a ts getAPSet
            storeAndReturn <| satExistsNext ts av
        | .Always a => do
            let av ← genSubFormulae a ts getAPSet
            storeAndReturn <| satExistsAlways ts av
        | .Until a b => do
            let av ← genSubFormulae a ts getAPSet
            let bv ← genSubFormulae b ts getAPSet
            storeAndReturn <| satExistsUntil ts av bv

-- evaluate a given subformula, producing a list of graph vertices that satisfy the subformula. You need to provide a function that
-- can evaluate atomic propositions of type A. Runs as a state monad, with the state
-- caching all previously evaluated subformulae
def genSubFormulae {A VI VL EL : Type} [BEq A] [BEq VI] (c : CTLENF A .State) (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) : StateM (CTLSubFormulae A (List VI)) (List VI) := do
    -- is this subformula alrady evaluated? check the state
    let sf : CTLSubFormulae A (List VI) ← get
    match lookupSubformula c sf with
    | Option.some v => pure v
    | Option.none => do
        -- Once you've computed a solution, use this instead of just (pure <solution>).
        -- This stores the result into state so we don't need to compute it again. (memoize)
        let storeAndReturn : List VI → StateM (CTLSubFormulae A (List VI)) (List VI) :=
            fun v => do
                modifyGet (fun sf => ⟨(),addSubformula c v sf⟩)
                pure v
        -- generate a value for this
        match c with
        -- true satisfies all vertices of the graph
        | .True => do
            let v := List.map ts.vertexFor (listRangeFin ts.vertexCount)
            storeAndReturn v
        -- use the provided function to map from AP to a satifiability set
        | .AP a => do
            storeAndReturn <| getAPSet ts a
        | .And a b => do
            let av ← genSubFormulae a ts getAPSet
            let bv ← genSubFormulae b ts getAPSet
            storeAndReturn <| av.filter bv.contains   -- a ∩ b
        | .Not a => do
            let av ← genSubFormulae a ts getAPSet
            -- the complement is all vertices not in av
            let notav := List.filterMap 
                          (fun i => let vi := ts.vertexFor i
                                    if av.contains vi then Option.none else Option.some vi)
                          (listRangeFin ts.vertexCount)
            storeAndReturn notav
        | .Exists p => genSubFormulaePath p ts getAPSet
end
  termination_by genSubFormulae c _ _ => sizeOf c
                 genSubFormulaePath p _ _ => sizeOf p

-- find the satifiability set of a given CTLformula.
def satCTL {A VI VL EL : Type} [BEq A] [BEq VI] (c : CTLFormula A .State) (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) : List VI :=
    let initialStore : CTLSubFormulae A (List VI) := {stateF := [], pathF := []}
    let cENF := fromCTLtoENF c
    let satSet := Id.run <| StateT.run' (genSubFormulae cENF ts getAPSet) initialStore
    satSet

-- find the satifiability set of a given CTLformula, and also return the subformulae satisfiability sets
def satCTLSub {A VI VL EL : Type} [BEq A] [BEq VI]
        (c : CTLFormula A .State) (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) : (List VI × CTLSubFormulae A (List VI)) :=
    let initialStore : CTLSubFormulae A (List VI) := {stateF := [], pathF := []}
    let cENF := fromCTLtoENF c
    StateT.run (genSubFormulae cENF ts getAPSet) initialStore

-- Find satifiability for a given AP from a APBits, where the transition system is already labeled using APBits
-- You pass in the value as the raw AP (i.e., a String or whatever) which is converted into APBits and checked against
-- the transition system.
def satAPBits {A VI EL : Type} {apList : List A} [BEq A] [BEq VI]
        (ts : FSM VI (APBits apList) EL) (ap : A) : List VI :=
    let b := @valToAPBits A apList _ ap
    let allVerts := List.map ts.vertexFor (listRangeFin ts.vertexCount)
    allVerts.filter
        (fun vi => let vbits := (ts.edgesFor vi).vl
                   (vbits.bits &&& b.bits) > 0)

-- check a transitionn system using CTL with a given function to generate satifiability sets from a specific AP  of type A.
def checkCTLAP {A VI VL EL : Type} [BEq A] [BEq VI] (ts : FSM VI VL EL) (c : CTLFormula A .State) (getAPSet : FSM VI VL EL → A → List VI) : Bool :=
    let satSet := satCTL c ts getAPSet
    -- all of the start states need to be in the satisfiability set
    ts.startStates.all satSet.contains

-- check a transition system expressed using APBits.  If you have your own representation of atomic propositions you can
-- use checkCTLAP
def checkUsingCTL {A VI EL : Type} {apList : List A} [BEq A] [BEq VI] (ts : FSM VI (APBits apList) EL) (c : CTLFormula A .State) : Bool  :=
    checkCTLAP ts c satAPBits

inductive CTLResult VI where
| Satisfies : CTLResult VI
| CounterExamplePath : List VI → CTLResult VI
| CounterExamplePathToSCC : List VI → List VI → CTLResult VI
| NoWitness : CTLResult VI
| Error : String → CTLResult VI
    deriving Repr

-- generate counter example from state `s` for a CTL formula of the form `a ⋃ b`
def genUntilCounterExampleAt {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [ToString A]
        (a b : CTLFormula A .State) (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) (s : VI) 
        : StateM (CTLSubFormulae A (List VI)) (CTLResult VI) := do
    -- for Until we generate a reduced graph, only keeping edges from vertices that satisfy (a ∧ ¬b)
    -- and then look for a nontrivial SCC or a terminal node that is (¬a ∧ ¬b)
    let eENF := fromCTLtoENF (.And a (.Not b))
    let satSet ← genSubFormulae eENF ts getAPSet
    let filteredGraph := GraphUtilities.filterEdges ts.toLabeledGraph (fun v₁ _ _ => satSet.contains v₁)
    -- try to find a path to one of the nontrivial SCCs of this reduced graph
    let sccs := GraphUtilities.findNontrivialSCCAt filteredGraph s
    let pathToSCC : CTLResult VI :=
        sccs.foldl 
            (fun p scc =>
                match p with
                | .Satisfies => match GraphUtilities.findPath filteredGraph s scc with
                            | .none => .Satisfies
                            | .some p => .CounterExamplePathToSCC p scc -- include the SCC in the counterexample path
                | _ => p)
            .Satisfies
    match pathToSCC with
    | .Satisfies => do
        -- couldn't find a path to a SCC
        -- the other option is to find a path to a node that satisfies (¬a ∧ ¬b)
        let tENF := fromCTLtoENF (.And (.Not a) (.Not b))
        let termSet ← genSubFormulae tENF ts getAPSet
        match GraphUtilities.findPath filteredGraph s termSet with
        | .some p => pure <| .CounterExamplePath p
        | .none => pure <| .Error ("Error, CTL expression " ++ toString (CTLFormula.Until a b) ++ " failed but no counterexample found")
    | _ => pure pathToSCC

-- finds a path refuting path formula `p` starting at the given state `s`. This is a counterexample
-- for the formula `∀p`
def genCounterExampleAt {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [ToString A]
        (p : CTLFormula A .Path) (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) (s : VI) 
        : StateM (CTLSubFormulae A (List VI)) (CTLResult VI) :=
    match p with
    | .Next e => do
        -- find a successor to s which does not satisfy e
        let eENF := fromCTLtoENF e
        let satSet ← genSubFormulae eENF ts getAPSet
        let succs := ts.edgesFor s
        match succs.el.find? (fun ⟨v,_⟩ => not (satSet.contains v)) with
        | .none => pure <| .Error ("Error, CTL expression " ++ toString p ++ " failed but no counterexample found")
        | .some ⟨s',_⟩ => pure <| .CounterExamplePath [s,s']
    | .Always e => do
        -- we need to find a path to a node that is `not e`
        let eENF := fromCTLtoENF (.Not e)
        let satSet ← genSubFormulae eENF ts getAPSet
        match GraphUtilities.findPath ts.toLabeledGraph s satSet with
        | .none => pure <| .Error ("Error, CTL expression " ++ toString p ++ " failed but no counterexample found")
        | .some s' => pure <| .CounterExamplePath s'
    | .Until a b => genUntilCounterExampleAt a b ts getAPSet s
    | .Eventually e => genUntilCounterExampleAt .True e ts getAPSet s


-- Checks CTL satifiability at a given state s.
-- If s doesn't satisfy the formula it attempts to generate a counterexample/witness for the given formula starting at state s. Runs in the same
-- state monad as genSubFormulae.
def checkCTLAt {A VI VL EL : Type} [BEq A] [BEq VI] [Ord VI] [ToString A]
        (c : CTLFormula A .State) (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) (s : VI) 
        : StateM (CTLSubFormulae A (List VI)) (CTLResult VI) :=
  do
    let cENF := fromCTLtoENF c
    let satSet ← genSubFormulae cENF ts getAPSet
    if satSet.contains s
    then pure .Satisfies
    else do
        -- look for a counterexample for ∀
        -- for ∃ we just say there isn't a witness
        -- anything else just returns the initial state
        match c with
        | .ForallPaths p => genCounterExampleAt p ts getAPSet s
        | .Not (.ExistsPath (.Always p)) => genCounterExampleAt /-.ForallPaths-/ (.Eventually (.Not p)) ts getAPSet s
        | .Not (.ExistsPath (.Eventually p)) => genCounterExampleAt /-.ForallPaths-/ (.Always (.Not p)) ts getAPSet s
        | .ExistsPath p => pure .NoWitness
        | _ => pure <| .CounterExamplePath [s]

-- check a transition system with CTL, if it fails then a counterexample is returned (if the expression is a ∀ or ¬∃).
def ctlCounterExample {A VI VL EL : Type} [BEq A] [ToString A] [BEq VI] [Ord VI]
        (c : CTLFormula A .State) (ts : FSM VI VL EL) (getAPSet : FSM VI VL EL → A → List VI) : CTLResult VI :=
    --let ⟨satSet, subFormulae⟩ := satCTLSub c ts getAPSet
    let initialStore : CTLSubFormulae A (List VI) := {stateF := [], pathF := []}
    --let cENF := fromCTLtoENF c
    let checkStartState :=
        fun res s =>
            -- if there was already an error/counterexample found, just return that one
            match res with
            | .Satisfies => do
                checkCTLAt c ts getAPSet s
            | _ => pure res
    -- check all start states to see if any fail and (if a ∀) generate a counterexample
    Id.run <| StateT.run' (ts.startStates.foldlM checkStartState .Satisfies)  initialStore
        

--#eval satCTL [ctl ∀◯(<+"green"+>∨<+"red"+>)] trafficLightLooping satAPBits
--#eval satCTLSub [ctl ∃□<+"green"+>] trafficLightLooping satAPBits
--#eval ctlCounterExample [ctl ∀□(<+"red"+>∨∀◯(<+"yellow"+>∨<+"green"+>))] trafficLightLooping satAPBits

end CTL
