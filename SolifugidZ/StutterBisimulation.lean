--
-- Divergent-sensitive stutter bisimulation quotienting of a transition system.
-- based on Chapter 7 of "Principles of Model Checking"
--

import Lean
import Lean.Data.RBMap
import LSpec

import SolifugidZ.Basic
import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.GraphUtilities
import SolifugidZ.FSM
import SolifugidZ.Bisimulation

namespace StutterBisimulation

open Basic

-- A given edge is a stutter-step if both the source and destination vertex have the
-- same label(s). Note that using `List String` as a vertex label will give bad results since
-- two lists of strings in a different order are not the same. Use `compareLists` from `Bisimulation`
-- if you really need to compare two lists of strings
def isStutterStep (g : LabeledGraph VI VL EL) (cmp : VL → VL → Ordering) (v₁ v₂ : VI) : Bool :=
    let vl₁ := VertexInfo.vl (g.edgesFor v₁)
    let vl₂ := VertexInfo.vl (g.edgesFor v₂)
    (cmp vl₁ vl₂ == Ordering.eq)

-- For a given graph, find strongly-connected components that are made up only of stutter-steps.
-- We include all vertices (including trivial SCCs) since we'll be  quotienting on this partition.
def findStutterCycles (g : LabeledGraph VI VL EL) (cmp : VL → VL → Ordering) [BEq VI] [Ord VI] : List (List VI) :=
    -- filter out all edges except stutter-steps
    let stutterGraph := GraphUtilities.filterEdges g (fun v₁ _  v₂ => isStutterStep g cmp v₁ v₂)
    GraphUtilities.findAllSCC stutterGraph

-- add divergence sensitivity: find all vertices with self-loops and route them to new node with
-- a self-loop, removing all other self-loops.
-- This adds a new vertex index .none and vertex label .none while putting all the original
-- values in .some
def addDivergenceNode (g : LabeledGraph VI VL EL) [BEq VI] [Ord VI] [Inhabited EL]: LabeledGraph (OOption VI) (OOption VL) EL :=
    -- new node is added on the end
    let newVertexCount := g.vertexCount + 1
    let newVertexFor : Fin newVertexCount → OOption VI :=
        fun ⟨ix,p⟩ =>
            if p₂ : ix < g.vertexCount
            then .Some <| g.vertexFor ⟨ix,p₂⟩
            else .None
    let newEdgesFor : OOption VI → VertexInfo (OOption VI) (OOption VL) EL
        | .None => VertexInfo.mk .None [⟨.None,default⟩]
        | .Some v =>
            let oldInfo := g.edgesFor v
            -- split edges into self-loops and not-self-loops
            let ⟨selfLoops, notSelfLoops⟩ := oldInfo.el.partition (fun ⟨v₂,_⟩ => v₂ == v)
            -- if there are self-loops we remove them and add an edge to the divergence node.
            -- otherwise we just return all the (not self) edges
            let notSelfLoops := notSelfLoops.map (fun ⟨v,el⟩ => ⟨.Some v, el⟩)
            if selfLoops.length > 0
            then VertexInfo.mk (.Some oldInfo.vl) (⟨.None,default⟩ :: notSelfLoops)
            else VertexInfo.mk (.Some oldInfo.vl) notSelfLoops
    {
        vertexCount := newVertexCount,
        vertexFor := newVertexFor,
        edgesFor := newEdgesFor
    }

-- prune .none states. If a node has a transition leading to the divergent state, that edge is removed
-- and replaced with a self-loop. Used to prune divergent nones added by addDivergenceNode
def pruneDivergenceNode (g : LabeledGraph (OOption VI) (OOption VL) EL) [BEq VI] [BEq EL] [Inhabited EL] [Inhabited VL] : LabeledGraph VI VL EL :=
    let remainingVerts := List.toArray <|
        Basic.foldFin g.vertexCount
        (fun l ix =>
            match g.vertexFor ix with
            | .None => l
            | .Some v => v :: l)
        []
    let newVertexCount := remainingVerts.size
    let newVertexFor := Array.get remainingVerts
    let newEdgesFor : VI → VertexInfo VI VL EL :=
        fun v₁ =>
            let vi := g.edgesFor (.Some v₁)
            let newVL := OOption.getD vi.vl default
            -- filter out transitions to .none (the divergent node) and
            -- replace them with self-loops
            let newEL := List.eraseDups <| vi.el.map (fun ⟨v₂,el⟩ => ⟨OOption.getD v₂ v₁,el⟩ )
            { vl := newVL, el := newEL }
    { 
        vertexCount := newVertexCount,
        vertexFor := newVertexFor,
        edgesFor := newEdgesFor
    }

def stutterRefine1Aux {VI VL EL : Type} [BEq VI] (g : LabeledGraph VI VL EL) (pathToC notPathToC : List VI) : List VI × List VI :=
    -- check the "not connected" set of vertices. If a vertex has a successor
    -- leading to the connected set that it also connected so we add it.
    -- we stop iterating once no more vertices are added to the connected set
    let ⟨newlyDiscovered, remaining⟩ :=
        notPathToC.partition <| fun v₁ =>
            let el := (g.edgesFor v₁).el
            el.any (fun ⟨v₂,_⟩ => pathToC.contains v₂)
    -- we keep recursing as long as we keep finding more vertices to add to pathtoC
    if remaining.length < notPathToC.length
    then
        stutterRefine1Aux g (pathToC ++ newlyDiscovered) remaining
    else ⟨pathToC,notPathToC⟩
  termination_by stutterRefine1Aux _ _ notPathToC => notPathToC.length

-- refine block B with respect to block C in some graph g.
-- We split B into two parts, one part made up of vertices in B with a path (in B) to C and "the rest"
def stutterRefine1 (g : LabeledGraph VI VL EL) (b c : List VI) [BEq VI] : List VI × List VI :=
    -- our initial split checks for vertices in B that connect to C
    let ⟨connectC, notConnectC⟩ :=
        b.partition <| fun v₁ =>
            let el := (g.edgesFor v₁).el
            el.any (fun ⟨v₂,_⟩ => c.contains v₂)
    stutterRefine1Aux g connectC notConnectC

-- try to refine a block
-- we basically try to refine the block w.r.t. all other blocks
-- returns .none if the block is stable (i.e., no split)
def stutterRefineBlock (g : LabeledGraph VI VL EL) (b : List VI) (blocks : List (List VI)) [BEq VI] : Option (Bisimulation.Refinement VI) :=
    blocks.firstM
        (fun c =>
        if b == c
        then .none
        else match stutterRefine1 g b c with
        | ⟨a,[]⟩ => .none
        | ⟨[],b⟩ => .none
        | ⟨p,np⟩ => .some ⟨b,p,np⟩)

-- examine all blocks in a partition to see if any will split under refinement
-- if a split occurs this returns the new partition. If no split occurs it returns .none
def stutterRefinePartition (g : LabeledGraph VI VL EL) (blocks : List (List VI)) [BEq VI] : Option (List (List VI)) :=
    match blocks.firstM (fun b => stutterRefineBlock g b blocks) with
    | .none => .none
    | .some ⟨orig,b₁,b₂⟩ =>
        -- replace the original with b₁ and cons on b₂
        let blocks := blocks.replace orig b₁
        .some (b₂ :: blocks)


-- repeatedly refine a partition until no changes occur
partial
def stutterRefinementLoop (g : LabeledGraph VI VL EL) (partition : List (List VI)) [BEq VI] : List (List VI) :=
    match stutterRefinePartition g partition with
    | .none => partition
    | .some p' => stutterRefinementLoop g p'

partial
def stutterPartition (g : LabeledGraph VI VL EL) (cmp : VL → VL → Ordering) [BEq VI] : List (List VI) :=
    let initial := Bisimulation.initialPartition g cmp
    -- convert from an RBMap into a list of blocks
    let p₀ := initial.fold (fun p _ blk => blk :: p) []
    stutterRefinementLoop g p₀

-- stutter-bisimulation quotienting with divergence sensitivity.
-- You need to provide a vertex index and label for the divergence state node
def stutterBisimulationDivQuotient (g : LabeledGraph VI VL EL) (cmp : VL → VL → Ordering) [BEq VI ] [Ord VI] [BEq EL] [Inhabited EL] [Inhabited VL] : LabeledGraph VI VL EL :=
    -- take a graph, compress stutter-cycles, and add a divergence state.
    let noStutterCycles := Bisimulation.quotientByPartition g (findStutterCycles g cmp) false
    let divergenceG := addDivergenceNode noStutterCycles
    -- partition the divergence-labeled graph
    let ocmp : OOption VL → OOption VL → Ordering
        | .None, .None => Ordering.eq
        | .None, .Some _ => Ordering.lt
        | .Some _, .None => Ordering.gt
        | .Some a, .Some b => cmp a b
    let newQuotient := Bisimulation.quotientByPartition divergenceG (stutterPartition divergenceG ocmp) true
    -- "restore" to the version without a divergence node
    pruneDivergenceNode newQuotient

end StutterBisimulation
