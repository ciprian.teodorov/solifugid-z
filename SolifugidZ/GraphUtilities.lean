import Lean

--
-- Some graph algorithms...
--  1. Strongly-connected components (via Tarjan's algorithm)
--  2. Depth-first search

import SolifugidZ.Basic
import SolifugidZ.LabeledGraph

open Basic
open Lean
open LabeledGraph

namespace GraphUtilities

--
-- Find Strongly-Connected Components (SCC) of a graph. Using Tarjan's method.
--

structure TJVertexData where
    (sequence : Nat) (lowlink : Nat)

structure TJState (VI : Type) [Ord VI] where
    (nextSequenceVal : Nat) (lowlink : RBMap VI TJVertexData compare) (stack : List VI) (visited : List VI) (sccList : List (List VI))

-- Recursive call to Tarjan's algorithm. We use a decreasing Nat to show well-founded recursion. Returns the lowest sequence index
-- of all vertices reachable from this vertex.
def dfsSCCaux {VI VL EL : Type} [BEq VI] [Ord VI] (g : LabeledGraph VI VL EL) (start : VI) (n : Nat) : StateM (TJState VI) Nat :=
    -- need this to prove termination
    match n with
    | Nat.zero => pure 0
    | Nat.succ nsucc => do
    -- get a sequence number for this vertex, and increment the counter we're using for that
    let s ← get
    let mySeq := s.nextSequenceVal
    let myStackIndex := s.stack.length -- might need this later to pop off a chunk of the stack
    set {s with 
        nextSequenceVal := s.nextSequenceVal +1,
        lowlink := s.lowlink.insert start ⟨mySeq,mySeq⟩,  -- initial lowlink is == to mySeq
        stack := s.stack ++ [start]}
    -- we'll iterate over all successor vertices
    let succ := List.map (fun x => x.1) (g.edgesFor start).el
    let lowlink ← List.foldlM
        (fun lowestSoFar v => do
            let ⟨_, lowlink, stack, visited, _⟩ ← get
            -- if we've already visited this node, skip it
            if visited.contains v
            then pure lowestSoFar
            else
            if stack.contains v
            -- if this vertex is on the stack compare the lowestSoFar value
            -- to the sequence value of the already visited vertex and return whichever is lower
            then match lowlink.find? v with
                | Option.none => pure lowestSoFar
                | Option.some ⟨seq,_⟩ => pure <| Nat.min lowestSoFar seq
            -- recurse into this vertex
            -- once it returns check the lowlink value
            else do
                let lowlinkChild ← dfsSCCaux g v nsucc
                pure <| Nat.min lowlinkChild lowestSoFar)
        mySeq
        succ
    if (lowlink == mySeq)
    -- this is the root of a subtree that is an SCC, pop off the tail of the stack as an SCC
    then do
        let s ← get
        let newSCC := s.stack.drop myStackIndex
        let newStack := s.stack.take myStackIndex
        set { s with
            stack := newStack,
            visited := s.visited ++ newSCC,
            sccList := newSCC :: s.sccList}
        pure mySeq
    -- not an SCC root, leave this node on the stack and update our lowlink value
    else do
        let s ← get
        set {s with
            lowlink := s.lowlink.insert start ⟨mySeq,lowlink⟩}
        pure lowlink
  termination_by dfsSCCaux _ _ n => n

-- Returns a list of strongly connected components.  Note that trivial SCCs of one vertex are included, you'll have to filter
-- them out.
--
-- We use Tarjan's method, preserving the set of visited vertices in a State Monad. This lets you
-- chain together multiple calls starting from different states, while remembering which vertices have already been visitied.
--
def dfsSCCm {VI VL EL : Type} [BEq VI] [Ord VI] (g : LabeledGraph VI VL EL) (start : VI) : StateM (List VI) (List (List VI)) := do
    -- we get the set of visited, and quick check that it hasn't been visited already
    let s ← get
    if s.contains start
    then pure []
    else
    -- run the scc with an initially empty stack and record of SCCs
    let ⟨_,s⟩ := StateT.run (dfsSCCaux g start g.vertexCount) ⟨0, RBMap.empty, [], s, []⟩
    set s.visited
    pure <| s.sccList

-- find strongly-connected components of a graph reachable from some node. Includes trivial SCCs (one node without a loop)
def findSCCAt {VI VL EL : Type} [BEq VI] [Ord VI] (g : LabeledGraph VI VL EL) (start : VI) : List (List VI) :=
    Id.run <| StateT.run' (dfsSCCm g start) []

def isNonTrivialSCC [BEq VI] (g : LabeledGraph VI VL EL) (scc : List VI) : Bool :=
    match scc with
    | List.nil => false -- scc is of size 0
    | List.cons _ (List.cons _ _) => true -- scc is of size > 1
    | List.cons h List.nil => 
           -- the scc is of size 1, which can be nontrivial if there is a self loop
           let e := (g.edgesFor h).el
           List.any e (fun ⟨v,_⟩ => v == h) 

-- find strongly-connected components of a graph, filtering out trivial SCCs (one node without a loop)
def findNontrivialSCCAt {VI VL EL : Type} [BEq VI] [Ord VI] (g : LabeledGraph VI VL EL) (start : VI) : List (List VI) :=
    let scc := Id.run <| StateT.run' (dfsSCCm g start) []
    scc.filter (fun v => isNonTrivialSCC g v)

-- find all strongly-connected components of a graph, filtering out trivial SCCs (one node without a loop)
-- Note that this checks every node of the graph. If a big chunk of your graph is not reachable from whatever
-- start state(s) you are using, it will find a bunch of SCCs you can't even reach. Consider filtering out
-- unreachable vertices first.
def findAllSCC {VI VL EL : Type} [BEq VI] [Ord VI] (g : LabeledGraph VI VL EL) : List (List VI) :=
    let sccAll := Id.run <| StateT.run'
        (Basic.foldFinM g.vertexCount (fun sccs ix => do
            let sccs2 ← dfsSCCm g (g.vertexFor ix)
            pure (sccs2 :: sccs))
            [])
        []
    sccAll.join
    
def findAllNontrivialSCC {VI VL EL : Type} [BEq VI] [Ord VI] (g : LabeledGraph VI VL EL) : List (List VI) :=
    (findAllSCC g).filter (isNonTrivialSCC g)

--
-- Depth-first search. Specify some start state and a set of goal states. Looks for a path from the start state s
-- to one of the goal states.  Specify the goals state with a function that returns `true` for any state in the goal.
-- Proving structural simulation of this is pretty rough, so I induce on a provided Nat called `fuel`
def dfsGraph [BEq VI]
    (ts : LabeledGraph VI VL EL) 
    (startNode : VI)
    (isGoalState : VI → Bool)
    (fuel : Nat) 
    : StateM (List VI) (Option (List VI)) :=
    if isGoalState startNode
    then pure [startNode]
    else if hf : fuel = 0
    then pure .none
    else do
    -- if we've already checked this node, fail
    let visitedNodes ← get
    if visitedNodes.contains startNode
    then pure .none
    else do
        set <| startNode :: visitedNodes
        let succs := ts.edgesFor startNode
        let checkNode := fun path ⟨node,_⟩ =>
            match path with
            | .some _ => pure path
            | .none => do
                have _ : Nat.pred fuel < fuel := Nat.pred_lt hf
                dfsGraph ts node isGoalState (fuel-1)
        match (← succs.el.foldlM checkNode Option.none) with
        | .none => pure .none
        | .some p => pure <| .some (startNode :: p)


-- Tries to find a path from a given start node to a node that satifies the goal predicate.
-- Returns a path if found, otherwise returns .none
def findPathPredicate [BEq VI]
    (g : LabeledGraph VI VL EL) 
    (startNode : VI)
    (goalPredicate : VI → Bool)
    : Option (List VI) :=
        Id.run <| StateT.run' (dfsGraph g startNode goalPredicate (g.vertexCount+1)) []

-- Tries to find a path from a given start node to any of the nodes in the goal list.
-- Returns a path if found, otherwise returns .none
def findPath [BEq VI]
    (g : LabeledGraph VI VL EL) 
    (startNode : VI)
    (goalStates : List VI)
    : Option (List VI) :=
        findPathPredicate g startNode goalStates.contains

--
-- Filter out edges in a graph. You provide a filter function that takes an edge as a tuple `⟨s,e,s'⟩`
-- discribing the edge from `s` to `s'` with a label `e`. The function returns true if this edge is to be kept.
--
def filterEdges [BEq VI]
    (g : LabeledGraph VI VL EL)
    (edgeFilter : VI → EL → VI → Bool)
    : LabeledGraph VI VL EL :=
    let newEdges := fun vix =>
        let oldEdges := g.edgesFor vix
        {
            vl := oldEdges.vl,
            el := oldEdges.el.filter (fun ⟨vix2,l⟩ => edgeFilter vix l vix2)
        }
    { g with edgesFor := newEdges }

end GraphUtilities

