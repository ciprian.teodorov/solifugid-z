import Lean

import SolifugidZ.Basic
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM

open Basic LabeledGraph FSM

-- We represent NFA's as an FSM, with the end states representing accept states.

def NFA (VI : Type) (VL : Type) (EL : Type) := FSM VI VL EL

namespace NFA

--
-- a quick DSL for making Regex
--

inductive NFA_RE (a : Type) where
| Lit : a → NFA_RE a
| Star : NFA_RE a → NFA_RE a
| Or : NFA_RE a → NFA_RE a → NFA_RE a
| Seq : NFA_RE a → NFA_RE a → NFA_RE a


declare_syntax_cat regex

syntax " <~ " term " ~> " : regex
syntax regex " | " regex : regex
syntax regex regex : regex
syntax " ( " regex " ) " : regex
syntax regex "*" : regex
syntax " [/ " regex " /] " : term

syntax regex "+" : regex

macro_rules
  | `([/ <~ $x:term ~> /]) => `(NFA_RE.Lit $x)
  | `([/$x:regex*/]) => `(NFA_RE.Star [/$x/])
  | `([/$x:regex+/]) => `(NFA_RE.Seq (NFA_RE.Lit [/$x/]) (NFA_RE.Star [/$x/]))
  | `([/$x:regex | $y:regex/]) => `(NFA_RE.Or [/$x/] [/$y/])
  | `([/$x:regex $y:regex/]) => `(NFA_RE.Seq [/$x/] [/$y/])
  | `([/($x:regex)/]) => `([/$x/])
  | `([/$x:regex/]) => `([/$x/])

#check [/ (<~3~> (<~4~> | <~5~>))* <~9~>/]

--
-- construct NFA from a Regex tree
--

def regexToNFA {a :Type} [BEq a] (n : NFA_RE a) : NFA Nat String a :=
    match n with
    | .Lit x => 
        -- build a single-transition NFA for x
        {
            toLabeledGraph := fromExplicit <| 
                compileGraph [] [⟨0,1,x⟩],
            startStates := [0],
            endStates := [1]
        }
    | .Star x =>
       let xNFA := regexToNFA x
       -- need to loop the NFA. We do this by connecting the NFA to itself
       -- also start states are accept states since 0 matches is acceptable
       {
            toLabeledGraph := sequenceConnect xNFA.toLabeledGraph xNFA.endStates xNFA.startStates,
            startStates := xNFA.startStates,
            endStates := List.eraseDups <| xNFA.endStates ++ xNFA.startStates
       }
    | .Or x y => compactFSM <| mergeFSMs (regexToNFA x) (regexToNFA y)
    | .Seq a b => compactFSM <| sequenceFSMs (regexToNFA a) (regexToNFA b)

--#eval graphVizFSM <| onlyReachableFSM <| regexToNFA [/<~2~>(<~3~>(<~4~>|<~5~>))*<~9~>/]

-- used by productNFA to compute ends states of a product
def allEndProducts (ts : FSM TS_I VL EL) (e : NFA_I) (i : Fin ts.vertexCount) : List (OProd TS_I NFA_I) :=
    if hz : i.val = 0
    then [OProd.mk (ts.vertexFor i) e]
    else let i' : Fin ts.vertexCount := Fin.mk (i.val-1) (Nat.lt_trans (Nat.pred_lt hz) i.isLt)
        let tsi := ts.vertexFor i
        have h : i.val-1+1 < i.val+1 := by apply Nat.add_lt_add_right; apply Nat.pred_lt hz
        (OProd.mk tsi e) :: allEndProducts ts e i'
    termination_by allEndProducts _ _ i => i

--
-- combine an NFA with a transition system. In this case the NFA edge labels need to match
-- the Transition System Vertex Labels. A given vertex label triggers the NFA transition edge
-- with a matching label (if any).
--
def productNFA {TS_I NFA_I : Type} {VL EL NL : Type} [BEq VL] [Inhabited VL] [Inhabited NL] (ts : FSM TS_I VL EL) (nfa : NFA NFA_I NL VL) : FSM (Basic.OProd TS_I NFA_I) (VL × NL) EL :=
    if hz₁ : ts.vertexCount = 0
    then FSM.empty
    else if hz₂ : nfa.vertexCount = 0
    then FSM.empty
    else
      let vc := ts.vertexCount * nfa.vertexCount
      let gv := LabeledGraph.finProduct ts.vertexFor nfa.vertexFor hz₁ hz₂
      let ef := fun (i : OProd TS_I NFA_I) =>
          let tsData := ts.edgesFor i.1
          let nfaData := nfa.edgesFor i.2
          -- apply nfa matches to all ts edges
          let newEdges := List.join <| tsData.2.map
              fun ⟨tsi, ol⟩ =>
                  let ⟨nextLabel,_⟩ := ts.edgesFor tsi
                  nfaData.2.filterMap
                      fun ⟨nfai,nl⟩ =>
                          if nl == nextLabel
                                             then Option.some ⟨OProd.mk tsi nfai,ol⟩
                                             else Option.none
          VertexInfo.mk ⟨tsData.1,nfaData.1⟩ newEdges
      let lg := LabeledGraph.mk vc gv ef
      -- initial states are initial states of the TS with NFA transitions taking matching edges
      let initS := List.join <| ts.startStates.map
          fun tsi => 
            let tsData := ts.edgesFor tsi
            List.join <| nfa.startStates.map
                fun nfai =>
                    let nfaData := nfa.edgesFor nfai
                    let matchingEdges := nfaData.2.filter
                        (fun nfaE => nfaE.2 == tsData.1 )
                    matchingEdges.map (fun x => OProd.mk tsi x.1)
      -- end states are all product states where the second field is an end (accepting) state of the original NFA
      let endS := List.join <| nfa.endStates.map <| fun es => allEndProducts ts es (Fin.mk (ts.vertexCount-1) (Nat.pred_lt hz₁))
      {
        toLabeledGraph := lg,
        startStates := initS,
        endStates := endS
      }

def checkProductNFA_N
    [BEq VI] [BEq NL]
    (productNFA : FSM VI (VL × NL) EL) 
    (path : List (VL × EL))
    (curNode : VI)
    (fuel : Nat) 
    : StateM (List VI) (Option (VL × List (VL × EL))) := do
    -- so this is kind of a mess
    if hf : fuel = 0
    then pure Option.none
    else
    let acc ← get
    if acc.contains curNode
    then pure Option.none
    else do
      let ⟨nodeLabel, successorNodes⟩ := productNFA.edgesFor curNode
      --let path := nodeLabel.1 :: path
      if productNFA.endStates.contains curNode
      then pure <| Option.some ⟨nodeLabel.1, path⟩
      else do
        -- add this node to the accumulator
        MonadState.set (curNode :: acc)
        -- recurse into each successor node
        let checkNode := fun failPath ⟨node,el⟩ =>
            match failPath with
            | Option.some _ => pure failPath
            | Option.none =>
                have h : Nat.pred fuel < fuel := Nat.pred_lt hf
                let path := ⟨nodeLabel.1, el⟩ :: path
                checkProductNFA_N productNFA path node (Nat.pred fuel)
        successorNodes.foldlM checkNode Option.none
    -- right now we use vertex count to limit recursion and provide termination proof. There
    -- is probably a better way.
    termination_by checkProductNFA_N _ _ _ f => f

-- check the product NFA. We check to see if any reachable state corresponds to an accepting state of the NFA component.
-- If an accepting state is found, returns accepting state and a path to that state. Each element in the path is a tuple of the vertex label and
-- then an edge label of the transition that goes to the following vertex.
def checkProductNFA [BEq VI] [BEq NL] (productNFA : FSM VI (VL × NL) EL) : Option (VL × List (VL × EL)) :=
    let allStartStates := 
        (productNFA.startStates.foldlM
            (fun (failPath : Option (VL × List (VL × EL))) s =>
                match failPath with
                | Option.some _ => pure failPath
                | Option.none => checkProductNFA_N productNFA [] s productNFA.vertexCount)
            Option.none)
    let checkResult := @StateT.run' (List VI) Id _ (Option (VL × List (VL × EL))) allStartStates []
    -- the result is in reverse order
    match checkResult with
    | Option.none => Option.none
    | Option.some ⟨s,l⟩ => Option.some <| ⟨s, List.reverse l⟩

-- check if the given transition system has a path that accepted by the nfa. Return Option.none
-- if no path is found, or a path satifying the nfa if one is found.
def acceptingPathExists [BEq TS_I] [BEq NFA_I] [Inhabited VL] [BEq VL] [Inhabited NL] [BEq NL] (ts : FSM TS_I VL EL) (nfa : NFA NFA_I NL VL) : Option (VL × List (VL × EL)) :=
    let product := productNFA ts nfa
    checkProductNFA product

-- generate a FSM representing the finite word. This can be used with checkWithNFA to see if a particular word
-- is accepted by the NFA. Used for testing mostly. 
def generateFSMFromWord {VL : Type} [Inhabited VL] (w : List VL) : FSM Nat VL String :=
    let vertices := List.zipWith Prod.mk (List.range w.length) w
    let edges := List.map (fun n => ⟨n, n+1, "step" ++ toString n⟩) (List.range <| w.length -1)
    {
        toLabeledGraph := fromExplicit <| compileGraph vertices edges,
        startStates := [0],
        endStates := [w.length - 1]
    }


end NFA
