--
-- Bisimulation quotienting of a transition system.
-- based on algorithm 30 (Chapter 7) of "Pinciples of Model Checking"
--

import Lean
import Lean.Data.RBMap
import LSpec

import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM

namespace Bisimulation

open Lean
open APBits

-- pull the given vertex from the graph, look at its label and add to the partition
def putVertexRec {VI VL EL : Type}
    (ts : LabeledGraph VI VL EL)
    (cmp : VL → VL → Ordering)
    (partition : RBMap VL (List VI) cmp)
    (ix : Fin ts.vertexCount)
    : RBMap VL (List VI) cmp :=
    let v := ts.vertexFor ix
    let vl := (ts.edgesFor v).vl
    let partition :=
        match partition.find? vl with
        | .none => partition.insert vl [v]
        | .some vs => partition.insert vl (v :: vs)
    if h : ix.val = 0
    then partition
    else
        let ni : Fin ts.vertexCount := ⟨Nat.pred ix.val,by apply Nat.lt_trans _ ix.isLt; exact Nat.pred_lt h⟩
        putVertexRec ts cmp partition ni
  termination_by putVertexRec _ _ ix => ix.val

-- Generate initial partitioning of a transition system.
-- Each initial partition is all nodes with an equivalent vertex label.
-- The Vertex label needs an ordering. Note that (List String) is not gonna work, since different
-- label orders are not equivalent. You can/should convert the vertex labels to APBits and then partition on that.
def initialPartition (ts : LabeledGraph VI VL EL) (cmp : VL → VL → Ordering) : RBMap VL (List VI) cmp :=
    if h : ts.vertexCount = 0
    then .empty
    else
    let lastV : Fin ts.vertexCount := ⟨Nat.pred ts.vertexCount, Nat.pred_lt h⟩
    putVertexRec ts cmp .empty lastV

--
-- If you absolutely must use `List String` as your vertex label then this function will provide
-- a compatible ordering. We convert both lists to APBits and then compare them
def compareLists {A : Type} {aps : List A} [BEq A] (a b : List A) : Ordering :=
    let aBits : APBits aps := listToAPBits a
    let bBits : APBits aps := listToAPBits b
    compare aBits bBits

-- refine block B with respect to block C in some graph g.
-- We split B into two parts, one part made up of vertices with no successors in C and "the rest"
def refine1 (g : LabeledGraph VI VL EL) (b c : List VI) [BEq VI] : List VI × List VI :=
    b.partition <| fun v₁ =>
        let el := (g.edgesFor v₁).el
        el.all (fun ⟨v₂,_⟩ => c.contains v₂)

structure Refinement (VI : Type) where
    (original : List VI)
    (noPre : List VI)
    (remainder : List VI)

-- try to refine a block
-- we basically try to refine the block w.r.t. all other blocks
-- returns .none if the block is stable (i.e., no split)
def refineBlock (g : LabeledGraph VI VL EL) (b : List VI) (blocks : List (List VI)) [BEq VI] : Option (Refinement VI) :=
    blocks.firstM
        (fun c =>
        match refine1 g b c with
        | ⟨a,[]⟩ => .none
        | ⟨[],b⟩ => .none
        | ⟨p,np⟩ => .some ⟨b,p,np⟩)

-- examine all blocks in a partition to see if any will split under refinement
-- if a split occurs this returns the new partition. If no split occurs it returns .none
def refinePartition (g : LabeledGraph VI VL EL) (blocks : List (List VI)) [BEq VI] : Option (List (List VI)) :=
    match blocks.firstM (fun b => refineBlock g b blocks) with
    | .none => .none
    | .some ⟨orig,b₁,b₂⟩ =>
        -- replace the original with b₁ and cons on b₂
        let blocks := blocks.replace orig b₁
        .some (b₂ :: blocks)

-- repeatedly refine a partition until no changes occur
partial
def refinementLoop (g : LabeledGraph VI VL EL) (partition : List (List VI)) [BEq VI] : List (List VI) :=
    match refinePartition g partition with
    | .none => partition
    | .some p' => refinementLoop g p'


-- given some directed graph, generate a bisimulation partitioning of the graph
-- based on algorithm 30 (Chapter 7) of "Principles of Model Checking"
partial
def bisimulationPartition (g : LabeledGraph VI VL EL) (cmp : VL → VL → Ordering) [BEq VI] : List (List VI) :=
    let initial := initialPartition g cmp
    -- convert from an RBMap into a list of blocks
    let p₀ := initial.fold (fun p _ blk => blk :: p) []
    refinementLoop g p₀


-- Build a quotient of a graph given some partitioning
-- Note that the quotienting here does not know of each block of vertices in the partition have
-- cycles or not. If they have cycles then the reduced graph should have self-loops.
-- By default quotiented blocks with >1 vertex will have self-loops. However, you can specify that the
def quotientByPartition (g : LabeledGraph VI VL EL) (partition : List (List VI)) (pruneSelfLoops? : Bool) [BEq VI] [BEq EL] [Inhabited VL] : LabeledGraph VI VL EL :=
    -- first we need a nonzero number of blocks in the partition and each block has to have >0 vertices
    if hpl : partition.length = 0
    then LabeledGraph.empty
    else
    let quotientVI := partition.filterMap List.head?
    if hq : quotientVI.length < partition.length
    then LabeledGraph.empty
    else
    -- the quotient'd graph has one vertex for each block in the partition
    let vertexCount := partition.length
    -- for indices we take the vertex index of the first vertex in each block
    let vertexFor : Fin vertexCount → VI := fun ⟨ix,h⟩ => quotientVI.get ⟨ix,by apply Nat.le_trans h _; rw [←Nat.not_gt_eq]; exact hq⟩
    -- For labels we use the label of the first vertex in each block. It shouldn't matter which we use since all labels
    -- in a block are (SHOULD BE) the same.
    -- For edges we use find all the outgoing edges of all vertices, and point them at the vertex index in the new graph
    let quotientedVertex : VI → VI := fun v =>
        -- find out which block contains the original vertex and return the first vertex of that block
        match partition.find? (fun blk => blk.contains v) with
        | .none => v -- wtf, should flag this
        | .some blk => blk.headD v
    let edgesFor : VI → VertexInfo VI VL EL := fun v =>
        let originfo := g.edgesFor v
        match partition.find? (fun blk => blk.contains v) with
        | .none => VertexInfo.mk originfo.vl []
        | .some blk =>
            let allEdges := List.eraseDups <| List.join <| blk.map (fun x => (g.edgesFor x).el.map (fun ⟨v,el⟩ => ⟨quotientedVertex v,el⟩))
            let allEdges :=
                if pruneSelfLoops?
                then allEdges.filter <| fun ⟨v₂,el⟩ => v != v₂
                else allEdges
            VertexInfo.mk originfo.vl allEdges
    LabeledGraph.mk vertexCount vertexFor edgesFor

end Bisimulation
