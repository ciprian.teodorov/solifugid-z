-- Represent a set of atomic propositions (AP) as bits in a Nat.
--
-- The parameter is a list of all the possible elements in the set. This can be any
-- type but strings are convenient. Example:
--
-- def CritSectionAP := APBits ["wait1","crit1","wait2","crit2"]
--
-- def conflict : CritSectionAP := APin "crit1" && APin "crit2"

import Lean

import SolifugidZ.Basic

open Basic

structure APBits {A : Type} (aps : List A) where
   (bits : Nat)
   deriving Repr

instance : HOr (APBits aps) (APBits aps) (APBits aps) where
    hOr := fun b₁ b₂ => APBits.mk (b₁.bits ||| b₂.bits)

instance : HAnd (APBits aps) (APBits aps) (APBits aps) where
    hAnd := fun b₁ b₂ => APBits.mk (b₁.bits &&& b₂.bits)

instance : BEq (APBits aps) where
    beq := fun b₁ b₂ => b₁.bits == b₂.bits

instance : Inhabited (APBits aps) where
    default := APBits.mk 0

instance : Ord (APBits aps) where
    compare := fun a b => compare a.bits b.bits
section APBits

variable {A : Type} {aps : List A}


def notAP (b : APBits aps) : APBits aps :=
    -- only flip the lower N bits, where N = length of aps
    APBits.mk (2 ^ aps.length - 1 - b.bits)

-- the empty set
def APBits.empty : APBits aps := APBits.mk 0

-- used by valToAPBits. We walk down the list of possible APs. As we walk down the bitVal is moved to the
-- next bit (*2)
def valToAPBitsAux [BEq A] (apVal : A) (bitVal : Nat) (bitChecks : List A) : APBits aps :=
    match bitChecks with
    | List.nil => APBits.mk 0   -- apVal isn't found in this set of APBits
    | List.cons h t =>
        if h == apVal
        then APBits.mk bitVal
        else valToAPBitsAux apVal (bitVal*2) t

-- given a single AP, convert into an APBits with a single bit set that
-- represents a set with only that AP
def valToAPBits [BEq A] (apVal : A) : APBits aps :=
    valToAPBitsAux apVal 1 aps

def decodeAPBitsAux [BEq A] (x : APBits aps) (n : Nat) : List A :=
    if hn: n < aps.length
    then if (x.bits &&& (2^n)) > 0
         then aps[n]'hn :: decodeAPBitsAux x (n+1)
         else decodeAPBitsAux x (n+1)
    else []
  termination_by decodeAPBitsAux _ n => aps.length - n

-- converts an APBits to a list of A, which holds all AP included in this APBits
def decodeAPBits [BEq A] (x : APBits aps) : List A :=
    decodeAPBitsAux x 0


-- given a list of AP, generates an APBits with multiple bits set to represent the
-- set membership.
def listToAPBits [BEq A] (bits : List A) : APBits aps :=
   bits.foldl (fun s a => s ||| @valToAPBits A aps _ a) APBits.empty



-- A 'set of sets' this is a set of sets of AP.  Used for specifying branching
-- in NFAs. For example if you need all sets where "crit1" is true.
structure APBitSets (aps : List A) where
    (bitsets : List (APBits aps))
    deriving Repr

-- produce a powerset of AP (all possible subsets of the elements of AP)
def powerSet : List (APBits aps) := List.map APBits.mk <| List.range (2 ^ aps.length)

instance : HOr (APBitSets aps) (APBitSets aps) (APBitSets aps) where
    hOr := fun s₁ s₂ => APBitSets.mk <| List.eraseDups <| s₁.bitsets ++ s₂.bitsets

instance : HAnd (APBitSets aps) (APBitSets aps) (APBitSets aps) where
    hAnd := fun s₁ s₂ => APBitSets.mk <| 
                listProductFilter s₁.bitsets s₂.bitsets
                    (fun b₁ b₂ => if b₁ == b₂ then (Option.some b₁) else Option.none)

def bitNot (s : APBitSets aps) : APBitSets aps := APBitSets.mk <| powerSet.filter (fun x => not (s.bitsets.contains x))

-- Produce a set of all subsets that contain X
-- We make aps non-optional since type inference almost never works when aps is implicit
def trueAP [BEq A] (aps : List A) (x : A) : APBitSets aps :=
    let onlyX := @valToAPBits A aps _ x
    APBitSets.mk <| List.filter (fun y => (onlyX.bits &&& y.bits) > 0) <| powerSet

--def sampleAP := ["wait1","crit1","wait2","crit2"]
--#eval decodeAPBits (valToAPBits "crit1" : APBits sampleAP)
--#eval (trueAP sampleAP "crit1" &&& trueAP sampleAP "wait1" : APBitSets sampleAP).bitsets



end APBits


