-- Widget to visualize graphs in the Lean infoview

import SolifugidZ.Basic
import SolifugidZ.FSM
import SolifugidZ.ProgramGraph
import SolifugidZ.LTL

import Examples.CriticalSection

import Lean

open Lean Elab Widget

open FSM ProgramGraph

namespace VizGraph

@[widget]
def vizGraph : UserWidgetDefinition := {
  name := "Visualize Graph",
  -- You'll have to build this .js file from vizWidget.tsx before you can use the widget.
  -- This is built either by lake (lake build vizTarget) or by
  -- running the bundler manually in the /widget directory "npm run build -- --tsxName vizWidget"
  -- Or you can use "npm run watch -- --tsxName vizWidget" for rapid recompiling of the widget code during development
  javascript := include_str "widget/dist/vizWidget.js" 
}  

structure VizEdge where
    (dest : Nat) (label : String)
    deriving ToJson, FromJson, Inhabited

structure VizVertex where
    (index : Nat)
    (label : String)
    (outgoing : List VizEdge)
    deriving ToJson, FromJson, Inhabited

structure MarkInfo where
    (markLabel : String)
    (nodes : List Nat)
    deriving ToJson, FromJson, Inhabited

structure VizDetails where
    (vertices : List VizVertex)
    (startNodes : List Nat)
    (endNodes : List Nat)
    (markedNodes : List MarkInfo)
    deriving ToJson, FromJson, Inhabited

-- converts a FSM into a VizDetails structure so it can be sent to the vizWidget
-- You have to provide some functions to convert things into strings:
--  vertex labels, edge labels, start and end nodes
def toVizFSM
    (fsm : FSM VI VL EL)
    (vertexLabel : VL → String)
    (edgeLabel : EL → String)
    (startLabel : String)
    (endLabel : String)
    (markedVertices : List (String × List VI))
    [Ord VI] [BEq VI] [Inhabited VL] : VizDetails :=
    let ⟨natFSM, compactMapping⟩ := fsm.compactFSMWithMapping
    let natG := natFSM.toLabeledGraph
    let convertVertex := fun ix =>
        let vertexIndex := natG.vertexFor ix
        let vertexInfo := natG.edgesFor vertexIndex
        let newLabel := vertexLabel (vertexInfo.vl) ++
                        (if natFSM.startStates.contains vertexIndex then startLabel else "") ++
                        (if natFSM.endStates.contains vertexIndex then endLabel else "")
        let newEdges := vertexInfo.el.map <| fun ⟨n2,el⟩ => VizEdge.mk n2 (edgeLabel el)
        VizVertex.mk vertexIndex newLabel newEdges
    let vizList := List.map convertVertex (Basic.listRangeFin (natG.vertexCount))
    {
      vertices := vizList,
      startNodes := natFSM.startStates,
      endNodes := natFSM.endStates,
      markedNodes := markedVertices.map <| fun ⟨n,v⟩ => ⟨n, v.filterMap compactMapping⟩
    }

--
-- convert a ProgramGraph into a schematic graph visualization.
--
def toVizProgramGraph
    [Ord Loc]
    (pg : ProgramGraph Loc Action StateVariables)
    (actionLabel : Action → String)
    : VizDetails :=
    -- first we need to make a map from Loc to Nat. We gather all the nodes with outgoing transitions...
    let locMapping : RBMap Loc Nat compare := RBMap.fold (fun m k _ => m.insert k (m.size)) RBMap.empty pg.transitions
    -- There may be some nodes with no outgoing transitions, which means they have no key in the pg.transitions
    -- map and were not found by the previous fold. So we have to check all transition destinations
    -- for any Loc's that were not previously found
    let checkDestinations :=
        fun x _ v => v.foldl (fun m t => 
            match m.find? t.4 with
            | .none => m.insert t.4 m.size
            | .some _ => m) x
    let locMapping := RBMap.fold checkDestinations locMapping pg.transitions
    let processVertex : Loc → List (ProgramTransition Loc Action StateVariables) → VizVertex :=
        fun l transitions =>
            let vertexIndex := locMapping.findD l 0
            let label := String.intercalate "::" <| pg.locationLabels.findD l []
            let edges := transitions.map (fun t => VizEdge.mk (locMapping.findD t.4 0) (actionLabel t.2))
            VizVertex.mk vertexIndex label edges
    {
        vertices := locMapping.fold
                        (fun l k v => 
                            let label := String.intercalate "::" <| pg.locationLabels.findD k []
                            match pg.transitions.find? k with
                            | .none => (VizVertex.mk v label []) :: l
                            | .some edges => (processVertex k edges) :: l)
                        [],
        startNodes := pg.starts.map (fun l => locMapping.findD l 0),
        endNodes := [],
        markedNodes := []
    }

-- given some FSM and LTL expressions, this checks that the FSM satisfies the LTL expressions.
-- if a counterexample is found for an expression it is rendered in the infoview graph visualization. NOTE only the cyclic
-- part of the counterexample is shown! If no counterexample is found it renders a graph with nothing marked.
def vizLTLTest
    {A : Type} [BEq A] [BEq VI] [ToString A] [Ord VI] [ToString EL] {aps : List A}
    (ts : FSM VI (APBits aps) EL)
    (ltlexpr : List (LTL.LTLExpression A)) : VizDetails :=
    let counterExamples :=
        ltlexpr.map <| fun expr =>
            match LTL.checkUsingLTL ts expr with
            | .CounterExample ⟨l₁,l₂⟩ =>
                let markedVerts := l₂.map (fun z => z.ix)
                ⟨"counterexample for " ++ LTL.ltlString expr,markedVerts⟩
            | _ => ⟨"no counterexample for " ++ LTL.ltlString expr,[]⟩
    toVizFSM 
        (onlyReachableFSM <| ts) 
        (fun a => String.intercalate "/" (List.map toString (@decodeAPBits A aps _ a)))
        toString
        "start"
        "end"
        counterExamples
    

def infinitelyCrits := [ [ltl □◇<+"crit1"+>], [ltl □◇<+"crit2"+>] ]

#widget VizGraph.vizGraph (toJson <| vizLTLTest criticalSectionSystem infinitelyCrits)


/-
def p := toVizProgramGraph petProcess1 toString
    --(ProgramGraph.interleaveProgramGraphs petProcess1 petProcess2 .none)
    --(fun ⟨a,b⟩ => toString a ++ "/" ++ toString b)

def p2 : ProgramGraph Nat (ActionLabel String) Bool :=
    {
        transitions := RBMap.fromList [⟨0,[ProgramTransition.mk .none (.InfoText "test") id 1]⟩] compare,
        locationLabels := .empty,
        starts := [⟨0,false⟩]
    }

def p2viz := toVizProgramGraph p2 toString

#eval toJson p2viz
#widget VizGraph.vizGraph (toJson p2viz)
-/

end VizGraph
