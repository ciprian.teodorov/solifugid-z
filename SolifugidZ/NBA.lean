import Lean

import SolifugidZ.Basic
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.NFA

open LabeledGraph FSM Basic

-- NBA's are represented as an FSM, same as NFA's

def NBA (VI : Type) (VL : Type) (EL : Type) := FSM VI VL EL

-- GNBA's discard/ignore the default end states of FSM and add a set of sets of accepting states
structure GNBA (VI : Type) (VL : Type) (EL : Type) extends FSM VI VL EL where
    (acceptingSets : List (List VI))



namespace NBA

def empty {VI VL EL : Type} [Inhabited VL] : NBA VI VL EL := FSM.empty

-- A ω-regular expression is similar to a regular expression with some differences.
-- While regular expressions match finite-length words, ω-regular expressions match infinite-length words
-- An ω-regular expression is (usually) expressed as two regular expressions : A B^ω
-- The meaning of this representation is that A B^ω matches a finite prefix using A, then
-- matches the rest of word by repeatedly matching B an infinite number of times.
structure OmegaRegex (a : Type) where
   (pfx : NFA.NFA_RE a) (ω_cycle: NFA.NFA_RE a)

-- given an ω-regular expression, build an NBA that accepts it
def convertOmegaRegexToNBA {a : Type} [BEq a] (ωR : OmegaRegex a) : NBA Nat String a :=
    -- first generate NFA's for the two parts
    let prefixNFA := NFA.regexToNFA ωR.pfx
    let secondNFA := NFA.regexToNFA ωR.ω_cycle
    -- to generate the cycle we add loops from the end to the start on the second NFA
    let cycleNFA := {
        toLabeledGraph := sequenceConnect secondNFA.toLabeledGraph secondNFA.endStates secondNFA.startStates,
        startStates := secondNFA.startStates,
        endStates := secondNFA.endStates
    }
    -- lastly we connect the prefix to the cycle
    compactFSM <| sequenceFSMs prefixNFA cycleNFA

-- Take the product of a given NBA with some transition system. You can then perform a reachability or search in the product
-- FSM
def productNBA {TS_I NBA_I : Type} {VL EL NL : Type} [BEq VL] [Inhabited VL] [Inhabited NL] (ts : FSM TS_I VL EL) (nba : NBA NBA_I NL VL) : FSM (Basic.OProd TS_I NBA_I) (VL × NL) EL :=
   NFA.productNFA ts nba



-- We can't explicitly write out words of infinite length so we fake it with a ω-regular style struct that
-- specifies two parts: one part is the finite prefix word and the other is a finite word repeated an inifite
-- number of times.
structure OmegaWord (a : Type) where
   (pfx : List a) (inf : List a)
   deriving Repr

instance : Functor OmegaWord where
    map := fun f ⟨pfx, inf⟩ => ⟨pfx.map f, inf.map f⟩

-- generate a transition system representing the given ω-word. Used mostly for testing.
def generateFSMfromOmegaWord {VL : Type} [Inhabited VL] (w : OmegaWord VL) : FSM Nat VL String :=
    let fsm₁ := NFA.generateFSMFromWord w.pfx
    -- sequenceConnect skips the initial state so we add an extra state at the beginning (that sequenceConnect will skip)
    let fsm₂ := NFA.generateFSMFromWord (default :: w.inf)
    -- to generate the cycle we add loops from the end to the start on the second NFA
    let cycleFSM := {
        toLabeledGraph := sequenceConnect fsm₂.toLabeledGraph fsm₂.endStates fsm₂.startStates,
        startStates := fsm₂.startStates,
        endStates := fsm₂.endStates
    }
    -- lastly we connect the prefix to the cycle
    compactFSM <| sequenceFSMs fsm₁ cycleFSM



--
-- for nested DFS we need TWO sets of "visited" vertices, one for the outer DFS and another for the inner DFS
structure NestedDFSVisited (VI : Type) where
    (outerVisited : List VI) (innerVisited : List VI)
      deriving Repr

structure NFSStep (VI : Type u) (VL : Type) (EL : Type) where
    (ix : VI) (lbl : VL) (elbl : EL)
      deriving Repr

def EmptyVisited (VI : Type) : NestedDFSVisited VI := NestedDFSVisited.mk [] []

def isInOuter {VI : Type} [BEq VI] (s : NestedDFSVisited VI) (x : VI) : Bool := s.outerVisited.contains x
def isInInner {VI : Type} [BEq VI] (s : NestedDFSVisited VI) (x : VI) : Bool := s.innerVisited.contains x
def addToOuter {VI : Type} (s : NestedDFSVisited VI) (x : VI) : NestedDFSVisited VI := {s with outerVisited := x :: s.outerVisited}
def addToInner {VI : Type} (s : NestedDFSVisited VI) (x : VI) : NestedDFSVisited VI := {s with innerVisited := x :: s.innerVisited}

def nestedDFSInner [BEq VI] [BEq NL]
    (productNBA : FSM VI (VL × NL) EL) 
    (path : List (NFSStep VI VL EL))
    (cycleStartNode : VI)
    (curNode : VI)
    (fuel : Nat) 
    : StateM (NestedDFSVisited VI) (Option (List (NFSStep VI VL EL))) := do
    if hf : fuel = 0
    then pure Option.none
    else
    let visited ← get
    if isInInner visited curNode
    then pure Option.none
    else do
        MonadState.set <| addToInner visited curNode
        -- recurse into each successor node
        let ⟨nodeLabel, successorNodes⟩ := productNBA.edgesFor curNode
        let checkNode := fun failPath ⟨node,el⟩ =>
            match failPath with
            | Option.some _ => pure failPath
            | Option.none =>
                let newPath := NFSStep.mk curNode nodeLabel.1 el :: path
                if node == cycleStartNode
                then pure newPath
                else do
                    have _ : Nat.pred fuel < fuel := Nat.pred_lt hf
                    nestedDFSInner productNBA newPath cycleStartNode node (Nat.pred fuel)
        successorNodes.foldlM checkNode Option.none
    -- right now we use vertex count to limit recursion and provide termination proof. There
    -- is probably a better way.
    termination_by nestedDFSInner _ _ _ _ f => f


def nestedDFSOuter [BEq VI] [BEq NL]
    (productNBA : FSM VI (VL × NL) EL) 
    (path : List (NFSStep VI VL EL))
    (curNode : VI)
    (fuel : Nat) 
    : StateM (NestedDFSVisited VI) (Option (OmegaWord (NFSStep VI VL EL))) := do
    -- so this is kind of a mess
    if hf : fuel = 0
    then pure <| Option.some <| OmegaWord.mk path [] --Option.none
    else
    let visited ← get
    if isInOuter visited curNode
    then pure Option.none
    else do
        -- add this node to the accumulator
        MonadState.set <| addToOuter visited curNode
        -- recurse into each successor node
        let ⟨nodeLabel, successorNodes⟩ := productNBA.edgesFor curNode
        let checkNode := fun failPath ⟨node,el⟩ =>
            match failPath with
            | Option.some _ => pure failPath
            | Option.none =>
                have _ : Nat.pred fuel < fuel := Nat.pred_lt hf
                nestedDFSOuter productNBA (NFSStep.mk curNode nodeLabel.1 el :: path) node (Nat.pred fuel)
        let successorsResult ← successorNodes.foldlM checkNode Option.none
        match successorsResult with
        | Option.some ω_Path => pure <| Option.some ω_Path
        | Option.none => do
            if productNBA.endStates.contains curNode
            then do
                let cycleResult ← nestedDFSInner productNBA [] curNode curNode productNBA.vertexCount
                match cycleResult with
                | Option.none => pure Option.none
                | Option.some cyclePath => pure <| OmegaWord.mk path cyclePath
            else pure Option.none
    -- right now we use vertex count to limit recursion and provide termination proof. There
    -- is probably a better way.
    termination_by nestedDFSOuter _ _ _ f => f



-- check the product NBA. We check to see if any reachable state corresponds to an accepting state of the NFA component.
-- If an accepting state is found, returns the path to that state. Each element in the path is a tuple of the vertex label and
-- then an (optional) edge label of the transition that goes to the following vertex.
def checkProductNBA [BEq VI] [BEq NL] (productNBA : FSM VI (VL × NL) EL) : Option (OmegaWord (NFSStep VI VL EL)) :=
    let allStartStates := 
        (productNBA.startStates.foldlM
            (fun (failPath : Option (OmegaWord (NFSStep VI VL EL))) s =>
                match failPath with
                | Option.some _ => pure failPath
                | Option.none => nestedDFSOuter productNBA [] s productNBA.vertexCount)
            Option.none)
    let checkResult := @StateT.run' (NestedDFSVisited VI) Id _ (Option (OmegaWord (NFSStep VI VL EL))) allStartStates (EmptyVisited VI)
    -- the results are in reverse order
    match checkResult with
    | Option.none => Option.none
    | Option.some ow => Option.some <| ⟨ow.pfx.reverse, ow.inf.reverse⟩

-- check if the given transition system has an infinite path that is accepted by the NBA. Return Option.none
-- if no path is found, or Option.some of a path (specified as an ω-word) accepted by the NBA if one is found.
def acceptingPathExists [BEq TS_I] [BEq NBA_I] [Inhabited VL] [BEq VL] [Inhabited NL] [BEq NL] (ts : FSM TS_I VL EL) (nba : NBA NBA_I NL VL) : Option (OmegaWord (NFSStep (OProd TS_I NBA_I) VL EL)) :=
    let product := productNBA ts nba
    checkProductNBA product

end NBA

namespace GNBA

def empty {VI VL EL : Type} [Inhabited VL] : GNBA VI VL EL := {
    toLabeledGraph := LabeledGraph.empty,
    startStates := [],
    endStates := [],
    acceptingSets := []
}

-- Convert from GNBA to NBA. We do this by making N instances (copies) of the NBA, where N is the number of
-- acceptance sets. Each instance represents one of the acceptance sets, and reaching an acceptance states
-- leads to the next instance. From theorem 4.56 in the textbook.
def convertGNBANZ [BEq VI] [BEq EL] [Inhabited VL] (g : GNBA VI VL EL) (nz : g.acceptingSets.length > 0) : NBA (OProd VI (Fin g.acceptingSets.length)) VL EL :=
    if hz : g.vertexCount = 0
    then FSM.empty
    else
    let copyCount := g.acceptingSets.length
    let oldGraph := g.toLabeledGraph
    let VI₂ : Type := OProd VI (Fin g.acceptingSets.length)
    -- start and accepting states are in instance 0
    let startStates0 : List VI₂ := g.startStates.map (fun v => ⟨v,Fin.mk 0 nz⟩)
    let endStates0 : List VI₂ := g.acceptingSets[0]!.map (fun v => ⟨v,Fin.mk 0 nz⟩)
    -- multiplex out the vertex index to the N instances
    let newVertexFor : Fin (oldGraph.vertexCount * copyCount) → VI₂ :=
        fun f =>
            let vertexNotZero : 0 < g.vertexCount := Nat.zero_lt_of_ne_zero hz
            let vi : VI := g.vertexFor ⟨f.val % g.vertexCount,Nat.mod_lt f.val vertexNotZero⟩
            let ixVal := f.val / g.vertexCount % copyCount
            let nbaIx : Fin copyCount := ⟨ixVal,Nat.mod_lt (f.val/g.vertexCount) nz⟩
            ⟨vi, nbaIx⟩
    -- transitions are preserved (going to the same NBA instance as the source) unless we are in an
    -- accepting state of the current copy. If we are, add in transitions
    -- to the start successors of the next instance
    let newEdgesFor : VI₂ → VertexInfo VI₂ VL EL :=
        fun ⟨vi,nbaIx⟩ =>
            let ⟨label, edges⟩ := g.edgesFor vi
            let targetInstance :=
                if ¬ (g.acceptingSets[nbaIx].contains vi)
                then nbaIx
                else Fin.mk ((nbaIx.val+1) % copyCount) (Nat.mod_lt (nbaIx.val+1) nz)
            let instanceEdges := edges.map (fun ⟨vi,el⟩ => ⟨⟨vi,targetInstance⟩,el⟩)
            ⟨label, instanceEdges⟩

    -- now put it all together. Start/end states only apply in instance zero, the rest of the
    -- acceptance sets are "compiled into" the closure for newEdgesFor where it generates transitions to the next instance
    {
        toLabeledGraph := {
            vertexCount := oldGraph.vertexCount * copyCount,
            vertexFor := newVertexFor,
            edgesFor := newEdgesFor
        },
        startStates := startStates0,
        endStates := endStates0
    }

def convertGNBAZ [BEq VI] [BEq EL] [Inhabited VL] (g : GNBA VI VL EL) : NBA VI VL EL :=
    -- if there are no accepting sets, all states are accepting
    let allEndStates := List.map g.vertexFor (Basic.listRangeFin (g.vertexCount))
    {
        toLabeledGraph := g.toLabeledGraph,
        startStates := g.startStates,
        endStates := allEndStates
    }

def convertGNBA [BEq VI] [Ord VI] [BEq EL] [Inhabited VL] (g : GNBA VI VL EL) : NBA Nat VL EL :=
    if gz : g.acceptingSets.length = 0
    then compactFSM <| convertGNBAZ g
    else compactFSM <| convertGNBANZ g (Nat.zero_lt_of_ne_zero gz)

end GNBA
