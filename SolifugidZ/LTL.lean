import Lean

import SolifugidZ.Basic
import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.NBA

open Basic
open APBits
open NBA

namespace LTL

--
-- LTL logic
--
-- First we set up a DSL to parse LTL expressions into an inductive structure, then
-- compile that into a GNBA
--
-- We support the traditional ∧, ∨  , and ¬ as well as □(square) and ◇(diw) and ⋃/U (until) from LTL.
--
--

inductive LTLExpression (A : Type) where
    | AP : A → LTLExpression A
    | Not : LTLExpression A → LTLExpression A
    | And : LTLExpression A → LTLExpression A → LTLExpression A
    | Or : LTLExpression A → LTLExpression A → LTLExpression A
    | Implies : LTLExpression A → LTLExpression A → LTLExpression A
    | Always : LTLExpression A → LTLExpression A
    | Eventually : LTLExpression A → LTLExpression A
    | Next : LTLExpression A → LTLExpression A
    | Until : LTLExpression A → LTLExpression A → LTLExpression A
    deriving Repr, Ord, BEq

def ltlString [ToString A] : LTLExpression A → String
| .AP a => "<+" ++ toString a ++ "+>"
| .Not e => "¬" ++ ltlString e
| .And a b => ltlString a ++ "∧" ++ ltlString b
| .Or a b => ltlString a ++ "∨" ++ ltlString b
| .Implies a b => ltlString a ++ "→" ++ ltlString b
| .Always e => "□" ++ ltlString e
| .Eventually e => "◇" ++ ltlString e
| .Next e => "◯" ++ ltlString e
| .Until a b => ltlString a ++ "⋃" ++ ltlString b

declare_syntax_cat ltl

syntax "[ltl" ltl "]" : term
syntax " <+ " term " +> " : ltl

syntax ltl "∧" ltl : ltl
syntax ltl "∨" ltl : ltl
syntax "¬" ltl : ltl
syntax "□" ltl : ltl
syntax "◇" ltl : ltl
syntax "◯" ltl : ltl
syntax ltl "→" ltl : ltl
syntax ltl "⋃" ltl : ltl
syntax ltl "U" ltl : ltl
syntax "(" ltl ")" : ltl

macro_rules
  | `([ltl <+ $t:term +> ]) => `(LTLExpression.AP $t:term)
  | `([ltl ¬ $a ]) => `(LTLExpression.Not [ltl $a ])
  | `([ltl $a ∧ $b ]) => `(LTLExpression.And [ltl $a ] [ltl $b ])
  | `([ltl $a ∨ $b ]) => `(LTLExpression.Or [ltl $a ] [ltl $b ])
  | `([ltl □ $a ]) => `(LTLExpression.Always [ltl $a ])
  | `([ltl ◇ $a ]) => `(LTLExpression.Eventually [ltl $a ])
  | `([ltl ◯ $a ]) => `(LTLExpression.Next [ltl $a ])
  | `([ltl $a → $b]) => `(LTLExpression.Implies [ltl $a] [ltl $b])
  | `([ltl $a ⋃ $b ]) => `(LTLExpression.Until [ltl $a ] [ltl $b ])
  | `([ltl $a U $b ]) => `(LTLExpression.Until [ltl $a ] [ltl $b ])
  | `([ltl ( $a ) ]) => `([ltl $a])


instance [ToString A] : ToString (LTLExpression A) where
    toString := ltlString

-- An LTL normal form which only contains True, the AP, and operators (∧,¬,◯,⋃)
inductive NormalLTL (A : Type) where
  | True : NormalLTL A
  | AP : A → NormalLTL A
  | And : NormalLTL A → NormalLTL A → NormalLTL A
  | Not : NormalLTL A → NormalLTL A
  | Next : NormalLTL A → NormalLTL A
  | Until : NormalLTL A → NormalLTL A → NormalLTL A
  deriving BEq, Repr, Ord

def readableNF [ToString A] : NormalLTL A → String
    | .True => "_True_"
    | .AP a => "_" ++ toString a ++ "_"
    | .And a b => readableNF a ++ "∧" ++ readableNF b
    | .Not a => "¬" ++ readableNF a
    | .Next a => "◯" ++ readableNF a
    | .Until a b => "(" ++ readableNF a ++ "⋃" ++ readableNF b ++ ")"

instance [ToString A] : ToString (NormalLTL A) where
    toString := readableNF

-- convert LTL to our normal form
def toNF : LTLExpression A → NormalLTL A
  | .AP e => .AP e
  | .Not a => .Not (toNF a)
  | .And a b => .And (toNF a) (toNF b)
  | .Or a b => .Not (.And (.Not <| toNF a) (.Not <| toNF b))   -- (a ∨ b) ↔ ¬(¬a ∧ ¬b)
  | .Implies a b => .Not (.And (toNF a) (.Not <| toNF b))    -- a → b is (¬a ∨ b)
  | .Always a => .Not (.Until .True (.Not (toNF a)))          -- □a  ↔  ¬◇¬a
  | .Eventually a => .Until .True (toNF a)
  | .Next a => .Next <| toNF a
  | .Until a b => .Until (toNF a) (toNF b)

-- attempt to normalize LTL form. Mainly we reduce:
--   ¬¬a >> a
--   ¬◯a >> ◯¬a
def reduceNF : NormalLTL A → NormalLTL A
  | .Not (.Next a) => .Next (reduceNF <| .Not a)
  | .Not (.Not a) => reduceNF a
  | .Not x => .Not (reduceNF x)
  | .And a b => .And (reduceNF a) (reduceNF b)
  | .Next a => .Next (reduceNF a)
  | .Until a b=> .Until (reduceNF a) (reduceNF b)
  | other => other

-- given some LTL expression return a list of subformulae
def subformulaeNF [BEq (NormalLTL A)]: NormalLTL A → List (NormalLTL A) := fun e =>
    match e with
    | .True => [e]
    | .AP _ => [e]
    | .And a b => List.eraseDups <| e :: (subformulaeNF a ++ subformulaeNF b)
    | .Not a => subformulaeNF a   -- when we add a we implicitly add ¬a as well, so ignore dangling not values
    | .Next a => List.eraseDups <| e :: subformulaeNF a
    | .Until a b => List.eraseDups <| e :: (subformulaeNF a ++ subformulaeNF b)

--#eval subformulaeNF <| toNF <| [ltl □ (<+"wait1"+> ⋃ <+"crit1"+>) ]

-- Represents a subset of the set of subformulae. Each subformula has a Boolean that represents if the
-- formula is included. If a is not included, then the inverse (¬a) IS included.
structure SubformulaeSet (A : Type) where
    (subformulae : List (NormalLTL A × Bool))
    deriving Repr, BEq



-- readable form of subset
instance [ToString (NormalLTL A)] : ToString (SubformulaeSet A) where
    toString
    | SubformulaeSet.mk sf =>
        String.intercalate 
            "," 
            (sf.map (fun ⟨f,v⟩ => if v then toString f else "¬" ++ toString f))


-- given a subset, removes any subformulae except for those of the form "◯a". Then strips off
-- one instance of ◯ from the remaining subformulae.
def onlyNext (sfset : SubformulaeSet A) : SubformulaeSet A :=
    SubformulaeSet.mk <| 
        sfset.subformulae.filterMap
            (fun ⟨f,v⟩ => match f with
                      | .Next f' => Option.some ⟨f',v⟩
                      | _ => Option.none)
            

-- Given a sub formula _A_ , returns true if _A_ is included in the subset and false if _¬A_ is included.
def getSubFormulaValue [BEq (NormalLTL A)] (sfset : SubformulaeSet A) : NormalLTL A → Option Bool
    | .Not e => Option.map not (getSubFormulaValue sfset e)
    | e => sfset.subformulae.lookup e


def andXY (x : Option Bool) (y : Option Bool) : Option Bool := do
   let x' ← x
   let y' ← y
   pure (x' && y')

-- Given a subset of subformulae, checks to make sure the subset is logically consistant (i.e, there are
-- no contradictions).
def isSubFormulaConsistent [BEq (NormalLTL A)] (sfset : SubformulaeSet A) (subF : NormalLTL A) : Option Bool := do
    let isIncluded := getSubFormulaValue sfset
    -- we run in the Option monad
    let v ← isIncluded subF
      match subF with
      -- If "True" is a subformulae then it must appear in every elementary set
      | .True => pure (v == true)
      -- AP are always self-consistant
      | .AP _ => pure true
      -- If ¬a appears, then a should not.
      | .Not a => do
          let vN ← isIncluded a
          pure (v == not vN)
      -- if a and b are included, then (a∧b) should also but included
      | .And a b => do
          let a' ← isIncluded a
          let b' ← isIncluded b
          pure (v == (a' && b'))
      -- for next we need to check the consistency of the next step
      | .Next a => let sfNext := onlyNext sfset
                   isSubFormulaConsistent sfNext a
      | .Until a b => do
          let a' ← isIncluded a
          let b' ← isIncluded b
          -- v represents _Until a b_ and b' represents _b_
          match v,b' with
          -- if _b_ is included then (_Until a b_ must be included
          | true, true => pure true
          | false, true => pure false
          -- if _Until a b_ is included and b is not, then a must be
          | true, false => pure a'
          -- no consistency problems if neither _b_ or _Until a b_ are not included
          | false, false => pure true


def isElementarySet [BEq (NormalLTL A)] (sf : SubformulaeSet A) : Option Bool :=
    sf.subformulae.foldlM
        (fun b ⟨f,_⟩ => do 
            let f' ← isSubFormulaConsistent sf f
            pure <| b && f')
        true

def natToSubSet (fs : List (NormalLTL A)) (n : Nat) : List (NormalLTL A × Bool) :=
    match fs with
    | List.nil => []
    | List.cons h t =>
        let included? := if (n % 2 == 0) then false else true
        ⟨h,included?⟩ :: natToSubSet t (n/2)

-- given a list of formulae in NormalLTL form, this computes all the elementary subsets
-- if those formula. That is, this includes every subset which is maximal and consistent.
def elementarySets [BEq (NormalLTL A)] (formulae : List (NormalLTL A)) : Option (List (SubformulaeSet A)) :=
    let totalSets := 2 ^ formulae.length
    let allSets := List.map (fun n => SubformulaeSet.mk (natToSubSet formulae n)) (List.range totalSets)
    allSets.foldlM
        (fun e ss => do  -- in the Option monad
            let v ← isElementarySet ss
            if v
            then ss :: e
            else e)
            [] 
    
def compactShowSubFormulae [ToString A] (sf : List (SubformulaeSet A)) : String :=
    String.intercalate " || " (List.map toString <| sf)

def x := elementarySets (subformulaeNF <| reduceNF <| toNF <| [ltl <+"a"+> ⋃ ((¬<+"a"+>)∧<+"b"+>) ])

--#eval compactShowSubFormulae (Option.getD x [])

-- check if values of the subformula (which should be present in both b and b') will
-- allow a transition from b to b'
def hasTransitionAux [BEq (NormalLTL A)] (b : SubformulaeSet A) (b' : SubformulaeSet A) (f : NormalLTL A) : Option Bool := do
    let v ← getSubFormulaValue b f
    match f with 
    | .Next f' =>
        -- if _◯a_ is in the current state b then _a_ must be in the next state b'
        -- and similarly for _¬◯a_
        let v' ← getSubFormulaValue b' f'
        pure (v == v')
    | .Until p₁ p₂ =>
        -- if _p₁⋃p₂_ is in b then either _p₂_ is in b or (_p₁_ is in b AND _p₁⋃p₂_ is in b')
        -- and similarly for _¬p₁⋃p₂_  (until expansion law)
        let p₁v ← getSubFormulaValue b p₁
        let p₂v ← getSubFormulaValue b p₂
        let uv' ← getSubFormulaValue b' f
        pure (v == (p₂v || (p₁v && uv')))
    -- most subformulae don't effect the transitions
    | _ => pure true

-- true if a transition should occur from the GNBA state represented by b to b'
-- requirements are list in theorem 5.37 "GNBA for LTL formula"
def hasTransition [BEq (NormalLTL A)] (b : SubformulaeSet A) (b' : SubformulaeSet A) : Option Bool := 
    List.allM (fun ⟨f,_⟩ => hasTransitionAux b b' f) b.subformulae

-- given a subformula and an APBits, see if they are consistent
def checkFormulaBits {A : Type} {aps : List A} [BEq A] (b : APBits aps) (f : NormalLTL A × Bool) : Bool :=
    match f.1 with
    -- if this AP is in the APBits, make sure the APBits value matches the value in the formula
    | .AP a => if aps.contains a
               then let formulaValue := f.2
                    let bitsValue := (decodeAPBits b).contains a
                    (formulaValue == bitsValue)
               else true
    | _ => true

-- Given a list of atomic propositions (apList : List A) and a SubformulaeSet of A (sf : SubformulaeSet A),
-- this produces a list of subsets (represented as APBits A) of apList that match with the
-- subformaulae of sf.  Basically, any AP terms in sf must have a value (included/not included) that
-- matches the state of the same AP in a given APBits
def matchingAPBits {A : Type} [BEq A] (apList : List A) (sf : SubformulaeSet A) : List (APBits apList) :=
    let checkBits := fun bits => sf.subformulae.all (checkFormulaBits bits)
    List.filter checkBits (@powerSet A apList)


-- given a list of AP, a list of states (represented by elementary sets), and a specific state,
-- find all transitions out of that specific state to the other states.
def allTransitionsOut {A : Type} [BEq A] (apList : List A) (elSet : List (SubformulaeSet A))
    (thisSet : SubformulaeSet A)
    : List (SubformulaeSet A × SubformulaeSet A × APBits apList) :=
    -- first find the allowed values of APBits that can transition out of this state
    let apOut := matchingAPBits apList thisSet
    -- find out which other states we need to transition to
    let dests := elSet.filter (fun s => Option.getD (hasTransition thisSet s) false)
    -- produce all possible combinations of AP sets and destinations
    listProduct apOut dests (fun bits f => ⟨thisSet,f,bits⟩)


def e := Option.getD 
             (elementarySets (subformulaeNF <| reduceNF <| toNF <| [ltl ◯<+"a"+>]))
             []
def e0 := Option.getD e[0]? (SubformulaeSet.mk [])

--#eval compactShowSubFormulae e
--#eval allTransitionsOut ["a"] e e0

-- Given a specific subformula of a SubFormulaeSet, figure out:
--  1. if this specific subformula generates an accepting set
--  2. what states (represented by SubFormulaeSets) are in this accepting set
def acceptingSetFor {A : Type} [BEq A] (elSet : List (SubformulaeSet A))
    (thisSet : NormalLTL A)
    : Option (List (SubformulaeSet A)) :=
    match thisSet with
    -- for a⋃b we include all states where a⋃b is not included OR b is included
    | .Until _ b => Option.some <| elSet.filter
        (fun ss => let untv := getSubFormulaValue ss thisSet
                   let bv   := getSubFormulaValue ss b
                   match untv, bv with
                   | Option.some u', Option.some b' => ((not u') || b')
                   | _,_ => false)
    -- only until subformulae generate accepting sets
    | _ => Option.none

def generateGNBA {A : Type} [BEq A] [ToString A] (apList : List A) (ltlexpr : LTLExpression A) : Option (GNBA Nat String (APBits apList)) := do
    -- convert to normal form and find elementary sets
    let formulaNF := reduceNF <| toNF ltlexpr
    let subformulae := subformulaeNF formulaNF
    let elSets ← elementarySets subformulae --<| subformulaeNF nf
    -- generate transitions
    let allTransitions := List.join <| elSets.map (fun s => allTransitionsOut apList elSets s)
    -- generate accepting sets and start states
    let accepting := subformulae.filterMap (fun sf => acceptingSetFor elSets sf)
    let starts := elSets.filter (fun f => Option.getD (getSubFormulaValue f formulaNF) false)
    -- we need to order the states, so map from SubformulaeSet to Nat
    let setMapping : SubformulaeSet A → Nat :=
        fun s => match elSets.toArray.indexOf? s with
                 | Option.none => 0
                 | Option.some z => z.1
    let natTransitions : List (Nat × Nat × APBits apList) := allTransitions.map (fun ⟨s₁,s₂,e⟩ => ⟨setMapping s₁,setMapping s₂,e⟩)
    let stateLabels : List (Nat × String) := elSets.map (fun s => ⟨setMapping s, toString s⟩)
    let egr : LabeledGraph.ExplicitGraph Nat String (APBits apList) := LabeledGraph.compileGraph stateLabels natTransitions
    let gr := LabeledGraph.fromExplicit egr
    let acceptingNat := List.map (fun l => List.map setMapping l) accepting
    let startsNat := starts.map setMapping
    pure <| { toLabeledGraph := gr, startStates := startsNat, endStates := [], acceptingSets := acceptingNat}


inductive LTLResult (A : Type) where
| Error : String → LTLResult A
| NoCounterExample : LTLResult A
| CounterExample : A → LTLResult A
    deriving Repr

-- Given an LTL expression, check the given transition system for a counterexample. Returns the path of the counter example
-- or Option.none if not counterexample is found.
def checkUsingLTL {A : Type} [BEq A] [BEq VI] [ToString A] {aps : List A} (ts : FSM VI (APBits aps) EL) (ltlexpr : LTLExpression A) : LTLResult (OmegaWord (NFSStep VI (APBits aps) EL)) :=
    -- We actually try look for the complement of the desired property. This way any "correct"
    -- solution we find is a counterexample to the original desired property.
    let ltlexpr := .Not ltlexpr
    match generateGNBA aps ltlexpr with
    | Option.none => .Error "Error generating GNBA from LTL expression"
    | Option.some gnba =>
        let nba := GNBA.convertGNBA gnba
        match NBA.acceptingPathExists ts nba with
        | Option.none => .NoCounterExample
        | Option.some ω_word =>
            -- we strip out the NBA vertex index from the return value since it's not useful
            let swizzle := (fun ⟨⟨vi,_⟩,ap,el⟩ => ⟨vi,ap,el⟩)
            .CounterExample <| Functor.map swizzle ω_word


/-
def emptyGNBA (apList : List A) : GNBA Nat String (APBits apList) :=  {toLabeledGraph := LabeledGraph.empty, startStates := [], endStates := [], acceptingSets := []}
def apList := ["a","b"]
def testGNBA : GNBA Nat String (APBits apList) := Option.getD (generateGNBA apList [ltl <+"a"+>⋃◯<+"b"+>]) (emptyGNBA apList)

#eval FSM.graphVizFSM <| testGNBA.toFSM
#eval testGNBA.acceptingSets
-/
end LTL
