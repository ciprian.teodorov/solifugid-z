import Lean
import LSpec

import SolifugidZ.LabeledGraph
import SolifugidZ.GraphUtilities
import SolifugidZ.Bisimulation
import SolifugidZ.CTL

import Examples.TrafficLight
import Examples.CriticalSection

import SolifugidZ.VizGraph

open Lean Widget
open LSpec
open LabeledGraph GraphUtilities Bisimulation CTL

def trafficLightMultiBP := bisimulationPartition trafficLightMulti.toLabeledGraph compare
def trafficLightMultiBQ := FSM.mk (quotientByPartition trafficLightMulti.toLabeledGraph trafficLightMultiBP true) [0] []

def tlViz (g : FSM Nat (APBits TrafficAPList) Unit) :=
    VizGraph.toVizFSM
        g
        (fun a => String.intercalate "/" (List.map toString (@decodeAPBits String _ _ a)))
        (fun _ => "")
        "start"
        "end"
        []

-- critical section system with only the "crit1" and "crit2" labels retained.
def csSystem := criticalSectionSystem.mapLabels (fun bits => bits &&& (@listToAPBits _ CriticalSectionAPList _ ["crit1","crit2"]))

def csViz :=
    VizGraph.toVizFSM
        csSystem
        (fun a => String.intercalate "/" (List.map toString (@decodeAPBits String _ _ a)))
        toString
        "start"
        "end"
        []

--#widget VizGraph.vizGraph (toJson (tlViz trafficLightMulti))
--#widget VizGraph.vizGraph (toJson (tlViz trafficLightMultiBQ))
--#widget VizGraph.vizGraph (toJson csViz)

def allPathsGreen := [ctl ∀□∀◇<+"green"+>]

--#eval checkUsingCTL trafficLightMultiBQ allPathsGreen

def testBisimulation : TestSeq :=
    group "Bisimulation" 
        <| test "multi-state traffic light has 6 states" (trafficLightMulti.toLabeledGraph.vertexCount == 6)
        <| test "multi-traffic light cycles through green" (checkUsingCTL trafficLightMulti allPathsGreen)
        <| test "bisimulation partition of multi-traffic light has 4 states" (trafficLightMultiBP.length == 4)
        <| test "quotiented multi-traffic light cycles through green" (checkUsingCTL trafficLightMultiBQ allPathsGreen)

--#lspec testBisimulation

def main :=
    lspecIO testBisimulation
