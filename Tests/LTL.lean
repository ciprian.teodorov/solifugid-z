--
-- Tests for LTL
--

import Lean
import LSpec

import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.NBA
import SolifugidZ.LTL
import SolifugidZ.VizGraph

import Examples.TrafficLight
import Examples.CriticalSection
import Examples.PetersonExclusion

open LSpec
open LabeledGraph FSM NBA APBits
open LTL


def expectSuccess : LTLResult A → Bool
| .Error _ => false
| .NoCounterExample => true
| .CounterExample _ => false

def expectFailure : LTLResult A → Bool
| .Error _ => false
| .NoCounterExample => false
| .CounterExample _ => true


-- infinitely often green
def infinitelyGreen := [ltl □◇<+"green"+>]

-- infinitely often crit1. The standard critical section system does NOT
-- guarantee this.
def infinitelyCrit1 := [ltl □◇<+"crit1"+>]

-- infinitely often crit1 OR crit2
def infinitelyCrit1OR2 := [ltl □◇<+"crit1"+> ∨ □◇<+"crit2"+>]

-- if stuck in noncrit1, eventually will move to wait1
def weakFairnessWait1 := [ltl (◇□<+"noncrit1"+>) → (□◇<+"wait1"+>)]

-- if infinitely often wait1, then infinitely often crit1
def strongFairnessCrit1 := [ltl (□◇<+"wait1"+>) → (□◇<+"crit1"+>)]

-- enforced weak and strong fairness to avoid process starvation in critical section system
def criticalNoStarve1 :=
    [ltl
        (◇□<+"noncrit1"+> → □◇<+"wait1"+>) ∧
        (□◇<+"wait1"+> → □◇<+"crit1"+>)
    ]

-- this takes forever
--#eval checkUsingLTL criticalSectionSystem (LTLExpression.Implies criticalNoStarve1 infinitelyCrit1)

def testLTL : TestSeq :=
    group "LTL Checker" 
        <| test "traffic light reaches green infinitely often" (expectSuccess <| LTL.checkUsingLTL trafficLightRedGreen infinitelyGreen)
        <| test "critical section system do NOT reach crit1 infinitely often" (expectFailure <| LTL.checkUsingLTL criticalSectionSystem infinitelyCrit1)
        <| test "critical section system reaches crit1 OR crit2 infinitely often" (expectSuccess <| LTL.checkUsingLTL criticalSectionSystem infinitelyCrit1OR2)
        <| test "critical section does not satisfy weak fairness for wait1" (expectFailure <| LTL.checkUsingLTL criticalSectionSystem weakFairnessWait1)
        <| test "critical section does not satisfy strong fairness for crit1" (expectFailure <| LTL.checkUsingLTL criticalSectionSystem strongFairnessCrit1)
        <| test "peterson system does NOT reach crit1 infinitely often" (expectFailure <| LTL.checkUsingLTL petersonSystem infinitelyCrit1)
        <| test "peterson system does NOT satisfy weak fairness for wait1" (expectFailure <| LTL.checkUsingLTL petersonSystem weakFairnessWait1)
        <| test "peterson system satisfies strong fairness for crit1" (expectSuccess <| LTL.checkUsingLTL petersonSystem strongFairnessCrit1)

--#eval graphVizFSM <| compactFSM <| (GNBA.convertGNBA <| alwaysGreenGNBA)
--#eval graphVizFSM <| compactFSM <| onlyReachableFSM <| NBA.productNBA trafficLightX (GNBA.convertGNBA <| alwaysGreenGNBA)

--#eval LTL.checkUsingLTL trafficLightRedGreen infinitelyGreen

--#lspec testLTL

def testLTLFair : TestSeq :=
    test "fairness applied to critical system should work" (expectSuccess <| checkUsingLTL criticalSectionSystem (LTLExpression.Implies criticalNoStarve1 infinitelyCrit1))

--#eval LTL.checkUsingLTL trafficLightRedGreen infinitelyGreen
--#eval LTL.checkUsingLTL criticalSectionSystem weakFairnessWait1
--#eval LTL.checkUsingLTL criticalSectionSystem strongFairnessCrit1
--#eval LTL.checkUsingLTL petersonSystem strongFairnessCrit1

def main :=
    lspecIO <| group "all LTL" <|
        .append testLTL testLTLFair