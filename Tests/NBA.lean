import Lean
import LSpec

import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.ProgramGraph
import SolifugidZ.FSM
import SolifugidZ.NFA
import SolifugidZ.NBA

import Examples.CriticalSection

open LSpec
open LabeledGraph FSM NFA NBA APBits
open ProgramGraph

def checkerNBA : NBA Nat Nat String := renumberVerticesFSM <| onlyReachableFSM <|
    convertOmegaRegexToNBA ⟨[/(<~"go"~>|<~"argh"~>)*/],[/<~"gocycle"~><~"ack"~>/]⟩

def testFSM := generateFSMfromOmegaWord ⟨["go","argh","go","argh"],["gocycle","ack"]⟩
def testProduct := productNBA testFSM checkerNBA

--#eval graphVizFSM <| compactFSM <| onlyReachableFSM <| checkerNBA
--#eval checkProductNBA testProduct

def testAgainstOmegaWord : OmegaWord String → Option (OmegaWord (NFSStep (Basic.OProd Nat Nat) String String)) := fun ow =>
    checkProductNBA <| productNBA (generateFSMfromOmegaWord ow) checkerNBA

def testNBABasics : TestSeq :=
    group "NBA Basics" 
        <| test "check NBA against matching sequence"     (Option.isSome <| testAgainstOmegaWord ⟨["argh"],["gocycle","ack"]⟩)
        <| test "check NBA against matching sequence"     (Option.isSome <| testAgainstOmegaWord ⟨["go","go"],["gocycle","ack","gocycle","ack"]⟩)
        <| test "check NBA against matching sequence"     (Option.isSome <| testAgainstOmegaWord ⟨["go","argh","go","argh"],["gocycle","ack"]⟩)
        <| test "check NBA against non-matching sequence" (Option.isNone <| testAgainstOmegaWord ⟨["go","argh","blorp"],["gocycle","ack"]⟩)
        <| test "check NBA against non-matching sequence" (Option.isNone <| testAgainstOmegaWord ⟨["go","argh","go"],["ack","ack","gocycle"]⟩)

--#lspec testNBABasics




-- NBA for "infinitely often (crit1 or crit2)"
def alwaysOrCritNBA {aps : List String} : FSM Nat Unit (APBits aps) :=
    let c1c2 := trueAP aps "crit1" ||| trueAP aps "crit2"
    let notc1c2 := bitNot c1c2
    {
        toLabeledGraph := fromExplicit <|
            compileGraph
                []
                (
                c1c2.bitsets.map (fun x => ⟨0,1,x⟩)
                ++
                notc1c2.bitsets.map (fun x => ⟨0,0,x⟩)
                ++
                c1c2.bitsets.map (fun x => ⟨1,1,x⟩)
                ++
                notc1c2.bitsets.map (fun x => ⟨1,0,x⟩)
                )
        ,
        startStates := [0],
        endStates := [1]
    }


-- NBA for "eventually never crit<n>"
def neverCritNBA {aps : List String} (n : Nat) : FSM Nat Unit (APBits aps) :=
    let crit := trueAP aps ("crit" ++ toString n)
    let notcrit := bitNot crit
    {
        toLabeledGraph := fromExplicit <|
            compileGraph
                []
                (
                powerSet.map (fun x => ⟨0,0,x⟩)
                ++
                notcrit.bitsets.map (fun x => ⟨0,1,x⟩)
                ++
                crit.bitsets.map (fun x => ⟨1,2,x⟩)
                ++
                notcrit.bitsets.map (fun x => ⟨1,1,x⟩)
                ++
                powerSet.map (fun x => ⟨2,2,x⟩)
                )
        ,
        startStates := [0],
        endStates := [1]
    }

--#eval graphVizFSM <| compactFSM <| onlyReachableFSM <| productNBA semaphoreSystem alwaysCritNBA
--#eval checkWithNBA semaphoreSystem alwaysCritNBA
--#eval checkWithNBA semaphoreSystem neitherCritNBA

def testNBAChecker : TestSeq :=
    group "NBA Checker" 
        <| test "system can reach crit1/crit2 infinitely often" (Option.isSome <| NBA.acceptingPathExists criticalSectionSystem alwaysOrCritNBA)
        <| test "system can eventually stop reaching crit1"  (Option.isSome <| NBA.acceptingPathExists criticalSectionSystem (neverCritNBA 1))
        <| test "system can eventually stop reaching crit2"  (Option.isSome <| NBA.acceptingPathExists criticalSectionSystem (neverCritNBA 2))

--#lspec testNBAChecker

--#eval checkWithNBA semaphoreSystem alwaysCritNBA


-- still need a good GNBA test

def main :=
    lspecIO 
        <| .append testNBABasics
        <| testNBAChecker

