--
-- Tests for CTL with fairness
--

import Lean
import LSpec

import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.CTL
import SolifugidZ.CTLFair

import Examples.TrafficLight
import Examples.CriticalSection
import Examples.PetersonExclusion

open LSpec
open LabeledGraph FSM APBits CTL CTLFair

-- we use fairness applied to the looping traffic light to keep it from sticking on green forever
--      □◇green → □◇yellow
def mustLeaveGreenWeak := [CTLFairness.Weak [ctl <+"green"+>] [ctl <+"yellow"+>]]
def mustLeaveGreenStrong := [CTLFairness.Strong [ctl <+"green"+>] [ctl <+"yellow"+>]]
def mustLeaveGreenUnconditional := [CTLFairness.Unconditional [ctl <+"yellow"+>]]

def mustLeaveGreenStrongCompiled := 
    StateT.run'
        (compileFairness trafficLightLooping mustLeaveGreenStrong satAPBits)
        (CTLSubFormulae.mk [] [])
    
def stuckOnGreen := [ctl ∃◇∃□<+"green"+>]
def stuckOnGreenSat := satCTL [ctl ∃□<+"green"+>] trafficLightLooping satAPBits
def stuckOnGreenFair := satExistsAlwaysFair trafficLightLooping mustLeaveGreenStrongCompiled (satCTL [ctl <+"green"+>] trafficLightLooping satAPBits)

--#eval mustLeaveGreen
--#eval satCTL [ctl ∃□(<+"noncrit1"+>)] petersonSystem satAPBits
--#eval stuckOnGreenSat
--#eval stuckOnGreenFair
--#eval satCTLFairSub [ctl ∃◇∃□<+"green"+>] trafficLightLooping mustLeaveGreenWeak satAPBits
--#eval fromCTLtoENF stuckOnGreen
--#eval satExistsAlwaysFair trafficLightLooping mustLeaveGreen stuckOnGreen



-- fairness constraints to prevent process 1 from cycling in noncrit1 forever or never getting
-- a chance to enter crit1 when waiting
def criticalNoStarve1 : List (CTLFairness (CTLFormula String .State)) := 
    [
        -- ◇□noncrit1 → □◇wait1
        .Weak [ctl <+"noncrit1"+>] [ctl <+"wait1"+>],
        -- □◇wait1 → □◇crit1
        .Strong [ctl <+"wait1"+>] [ctl<+"crit1"+>]
    ]

def alwaysEventuallyCrit1 := [ctl ∀□∀◇<+"crit1"+>]

--#eval satCTLSub alwaysEventuallyCrit1 criticalSectionSystem satAPBits
--#eval satCTLFairSub alwaysEventuallyCrit1 criticalSectionSystem criticalNoStarve1 satAPBits

def hasCounterExample : CTLResult a → Bool
| .CounterExamplePath _ => true
| .CounterExamplePathToSCC _ _ => true
| _ => true


def testCTLFair : TestSeq :=
    group "CTL Checker with fairness" 
        <| test "looping traffic light can get stuck on green (raw check)" (stuckOnGreenSat.length > 0)
        <| test "looping traffic light can't get stuck on green with mustLeaveGreen strong fairness (raw check)" (stuckOnGreenFair.length == 0)
        <| test "looping traffic light can get stuck on green" (checkUsingCTL trafficLightLooping stuckOnGreen)
        <| test "looping traffic light can't get stuck on green with mustLeaveGreen weak fairness" (checkUsingCTLFair trafficLightLooping stuckOnGreen mustLeaveGreenWeak == false)
        <| test "looping traffic light can't get stuck on green with mustLeaveGreen strong fairness" (checkUsingCTLFair trafficLightLooping stuckOnGreen mustLeaveGreenStrong == false)
        <| test "looping traffic light can't get stuck on green with mustLeaveGreen unconditional fairness" (checkUsingCTLFair trafficLightLooping stuckOnGreen mustLeaveGreenUnconditional == false)
        <| test "critical section without fairness does not always reach crit1" (checkUsingCTL criticalSectionSystem alwaysEventuallyCrit1 == false)
        <| test "critical section with fairness always reaches crit1" (checkUsingCTLFair criticalSectionSystem alwaysEventuallyCrit1 criticalNoStarve1)
        <| test "looping traffic light does not have a green loop under weak fairness" (hasCounterExample <| ctlCounterExampleFair [ctl ∀□(<+"green"+> → ∃□<+"green"+>)] mustLeaveGreenWeak trafficLightLooping satAPBits)

--#eval ctlCounterExample [ctl ∀□¬(<+"yellow"+>∧∀◯<+"yellow"+>)] trafficLightMulti satAPBits
--#eval ctlCounterExample [ctl ∀□(<+"green"+> → ∃□<+"green"+>)] trafficLightLooping satAPBits
--#eval ctlCounterExampleFair [ctl ∀□(<+"green"+> → ∃□<+"green"+>)] mustLeaveGreenWeak trafficLightLooping satAPBits

--#eval criticalSectionSystem.vertexCount
--#eval fsmSCC criticalSectionSystem

--#lspec testCTLFair

def main := lspecIO testCTLFair