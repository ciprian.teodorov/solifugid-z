import Lean
import LSpec

import SolifugidZ.LabeledGraph
import SolifugidZ.FSM

open LSpec
open LabeledGraph FSM

--
-- Tests for LabeledGraph and FSM
--
-- We use (Option String) for edge labels and say any .some value requires synchronous stepping
-- when taking synchronous products.


--
-- Basic Graph tests
--

-- graph with start/end labeled nodes and some named transition
def namedTransition (el : String) : LabeledGraph Nat String (Option String) :=
    fromExplicit <| compileGraph [⟨0,"start"⟩,⟨1,"end"⟩] [⟨0,1,.some el⟩]

-- two-node loop with start labeled
def twoNodeLoop (sl : String) : LabeledGraph Nat String (Option String) :=
    fromExplicit <| compileGraph [⟨0,sl⟩] [⟨0,1,.none⟩,⟨1,0,.none⟩]

-- three nodes in sequence, both transitions are named
def threeNodeSequence (m : String) (l₁ l₂ : String) : LabeledGraph Nat String (Option String) :=
    fromExplicit <| compileGraph
        [⟨0,"start" ++ m⟩, ⟨1,"center" ++ m⟩, ⟨2,"end" ++ m⟩]
        [⟨0,1,.some l₁⟩, ⟨1,2,.some l₂⟩]

def x := namedTransition "argh"
def x3 := threeNodeSequence "three" "go1" "go2"
def y := namedTransition "ack"


def testGraphBasics : TestSeq :=
    group "Graph Basics"
        <| test "compileGraph on nameTransition gives two nodes" (x.vertexCount = 2)
        <| test "compileGraph on twoNodeLoop gives two nodes" ((twoNodeLoop "test").vertexCount = 2)
        <| test "cartesian product multiplies node count" ((graphProduct x x).vertexCount = 4)
        <| test "cartesian product multiplies node count" ((graphProduct x x3).vertexCount = 6)
        <| test "merged graphs add node count" ((mergeGraphs x y).vertexCount = 4)
        <| test "merged graphs add node count" ((mergeGraphs x x3).vertexCount = 5)
        <| test "cartesian product still reaches all nodes" ((reachableVertices (graphProduct x x) ⟨0,0⟩).length = 4)
        <| test "sync product only reaches two nodes" ((reachableVertices (syncProduct x x Option.isSome) ⟨0,0⟩).length = 2)

--#lspec testGraphBasics

--
-- Basic FSM tests
--

def genTestFSM (nStr : String) (l₁ l₂ : String) : FSM Nat String (Option String) :=
    {
        toLabeledGraph := fromExplicit <| 
            compileGraph [⟨1,"center" ++ nStr⟩] [⟨0,1,Option.some l₁⟩, ⟨1,2,Option.some l₂⟩],
        startStates := [0],
        endStates := [2]
    }

def testFSM_A := genTestFSM "testA" "lockA" "unlockA"
def testFSM_B := genTestFSM "testB" "lockB" "unlockB"

def testFSMBasics : TestSeq :=
    group "FSM Basics"
        <| test "space filler" (3 = 3)
        <| test "test FSM A has three nodes" (testFSM_A.vertexCount = 3)
        <| test "test FSM B has three nodes" (testFSM_B.vertexCount = 3)
        <| test "FSM product has 9 nodes" ((productFSMs testFSM_A testFSM_B).vertexCount = 9)
        <| test "FSM matching sync product has three nodes" ((onlyReachableFSM <| syncProductFSMs testFSM_A testFSM_A Option.isSome).vertexCount = 3)
        <| test "FSM not matching sync product has one node" ((onlyReachableFSM <| syncProductFSMs testFSM_A testFSM_B Option.isSome).vertexCount = 1)
        <| test "FSM sequence has 6 nodes before compaction" ((sequenceFSMs testFSM_A testFSM_B).vertexCount = 6)
        <| test "FSM sequence has 5 nodes after compaction" ((onlyReachableFSM <| sequenceFSMs testFSM_A testFSM_B).vertexCount = 5)

--#lspec testFSMBasics

def main :=
    lspecIO
        <| .append testGraphBasics
        <| testFSMBasics
