--
-- Tests for CTL
--

import Lean
import LSpec

import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.CTL
import SolifugidZ.VizGraph

import Examples.TrafficLight
import Examples.CriticalSection
import Examples.PetersonExclusion

open LSpec
open LabeledGraph FSM APBits CTL

-- you can always reach green
def infinitelyGreen := [ctl ∀□∃◇<+"green"+>]

-- all paths reach green.
def allPathsGreen := [ctl ∀□∀◇<+"green"+>]

-- all paths reach crit1
def alwaysCrit1 := [ctl ∀◇<+"crit1"+>]

-- If process 1 is waiting it can always immediately enter the critical section (not that it will, just that it CAN)
-- unless the other process is in the critical section. Note that this is FALSE for the peterson system since it
-- forces processes to take turns accessing the critical section.
def immediateCrit1 := [ctl ∀□((<+"wait1"+> ∧ ¬<+"crit2"+>) → ∃◯<+"crit1"+>)]

-- there is always a path to reach crit1
def reachableCrit1 := [ctl ∀□∃◇<+"crit1"+>]

-- infinitely often crit1 OR infinitely often crit2
def reachableCrit1OR2 := [ctl ∀□∀◇<+"crit1"+> ∨ ∀□∀◇<+"crit2"+>]

def hasCounterExample : CTLResult a → Bool
| .CounterExamplePath _ => true
| .CounterExamplePathToSCC _ _ => true
| _ => true

def testCTL : TestSeq :=
    group "CTL Checker" 
        <| test "traffic light always has green reachable" (checkUsingCTL trafficLightRedGreen infinitelyGreen)
        <| test "traffic light always has green reachable" (checkUsingCTL trafficLightRedGreen allPathsGreen)
        <| test "looping traffic light does NOT always have green reachable" (checkUsingCTL trafficLightLooping allPathsGreen == false)
        <| test "critical section system always eventually reaches crit1 OR crit2" (checkUsingCTL criticalSectionSystem reachableCrit1OR2 == false)
        <| test "critical section system can always immediately enter the critical section if available" (checkUsingCTL criticalSectionSystem immediateCrit1)
        <| test "peterson system can always reach crit1" (checkUsingCTL petersonSystem reachableCrit1)
        <| test "peterson system cannot always immediately reach the critical section" (checkUsingCTL petersonSystem immediateCrit1 == false)
        <| test "looping light has a counterexample showing infinitely looping green" (hasCounterExample <| ctlCounterExample [ctl ∀□¬(∃□<+"green"+>)] trafficLightLooping satAPBits)
        <| test "multi-state light has a counterexample with two yellow states in a row" (hasCounterExample <| ctlCounterExample [ctl ∀□¬(<+"yellow"+>∧∀◯<+"yellow"+>)] trafficLightMulti satAPBits)

--#eval ctlCounterExample [ctl ∀□¬(<+"yellow"+>∧∀◯<+"yellow"+>)] trafficLightMulti satAPBits
--#eval ctlCounterExample [ctl ∀□¬(∃□<+"green"+>)] trafficLightLooping satAPBits

--#lspec testCTL

def main := lspecIO testCTL

