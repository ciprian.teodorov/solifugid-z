import Lean
import LSpec

import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.NFA

open LSpec
open LabeledGraph FSM NFA


def checkerNFA : NFA Nat Nat String := renumberVerticesFSM <| onlyReachableFSM <| regexToNFA [/<~"go"~>(<~"ack"~><~"zzz"~>)*<~"end"~>/]

--#eval graphVizFSM checkerNFA
--#eval graphVizFSM <| compactFSM <| onlyReachableFSM <| productNFA (_root_.testFSM 1) testNFA
--#eval checkProductNFA (productNFA (_root_.testFSM "argh") checkerNFA) checkerNFA.endStates

def checkNFA (w : List String) := acceptingPathExists (generateFSMFromWord w) checkerNFA

--#eval graphVizFSM <| generateFSMFromWord ["ack","argh","end"]
--#eval checkNFA ["go","ack","zzz","end"]
--#eval graphVizFSM <| generateFSMFromWord ["go","ack","zzz","end"]

def testNFABasics : TestSeq :=
    group "NFA Basics" 
        <| test "check NFA against matching sequence" (Option.isSome <| checkNFA ["go","ack","zzz","end"])
        <| test "check NFA against matching sequence" (Option.isSome <| checkNFA ["go","end"])
        <| test "check NFA against matching sequence" (Option.isSome <| checkNFA ["go","ack","zzz","ack","zzz","end"])
        <| test "check NFA against non-matching sequence" (Option.isNone <| checkNFA ["go"])
        <| test "check NFA against non-matching sequence" (Option.isNone <| checkNFA ["go","ack","end"])
        <| test "check NFA against non-matching sequence" (Option.isNone <| checkNFA ["go","ack","zzz"])

--#lspec testNFABasics

def main :=
    lspecIO 
        <| testNFABasics
