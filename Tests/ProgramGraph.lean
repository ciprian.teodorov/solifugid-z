import Lean
import LSpec

import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.ProgramGraph

open LSpec
open LabeledGraph FSM ProgramGraph

----------
--
-- Tests for ProgramGraph
--
---------

--
-- simple State structure for testing
--

structure StateVars where
   (x : Bool) (y : Fin 4)
   deriving Repr

instance : ToString (StateVars) where
    toString := fun s => toString s.x ++ "," ++ toString s.y

instance : Inhabited (StateVars) where
    default := StateVars.mk false (Fin.mk 0 (by simp))

instance : StateCardinality (StateVars) where
    sSize := 2 * 4
    genState := fun i =>
        let y := i.val % 4
        let hy : i.val % 4 < 4 := Nat.mod_lt i.val (by simp)
        let x := if i.val < 4 then true else false
        StateVars.mk x ⟨y,hy⟩

instance : BEq StateVars := {
    beq := fun ⟨x₁,y₁⟩ ⟨x₂,y₂⟩ => x₁ == x₂ && y₁ == y₂
}

instance : Ord StateVars := {
    compare := fun ⟨x₁,y₁⟩ ⟨x₂,y₂⟩ =>
        match compare x₁ x₂ with
        | Ordering.eq => compare y₁ y₂
        | z => z
}






/-
-- hand-built sequence. Maybe useful for debugging someday
def somSeq : ProgramSequence (ActionLabel String) (StateVars) :=
    .Bind
        (.Statement .none (fun _ => true) (.InfoText "x := true, y:= 0") (fun s => {s with x := true, y := 0}))
        (.DoUntil
          (.Bind
              (.Statement (.some "signal") (fun _ => true) (.Signal "argh") id)
              (.Wait (.InfoText "x = true") (fun s => s.x = true))
          )
          (.InfoText "y < 2")
          (fun s => s.y < 2))
def tr : ProgramGraph Nat (ActionLabel String) (StateVars) := compileSequence somSeq [default]
-/





def initState (s : StateVars) : StateVars := {s with x := true, y := 2}
def isXTrue (s : StateVars) : Bool := s.x

--
-- aww yeah it's like we're back in the 1970's
--
def somSeq1 : ProgramSequence (ActionLabel String) StateVars :=
        LABEL "start" // <[ initState ]>
    //> IF? (λs=>s.y > 0)
        THEN DO
               LABEL "countdown" // <[λs => {s with y := s.y - 1}]>
             UNTIL (λs => s.y == 0) ..DO
        ..IF
    //> END "done1" ..END

def somSeq2 (initX : Bool) : ProgramSequence (ActionLabel String) StateVars :=
        LABEL "reset Y" // <[λs => {s with x := initX, y := 0} ]>
    //> IF? (λs=>s.x)
        THEN <[λs => {s with y := 3}]>
        ..IF
    //> END "done2" ..END

def somSeq3 : ProgramSequence (ActionLabel String) StateVars :=
        <[ λs => {s with x:= false, y := s.y + 1}]>
    //> END "done3" ..END

def somSeq4 : ProgramSequence (ActionLabel String) StateVars :=
        <[λs => {s with x:= true, y := s.y * 2}]>
    //> END "done4" ..END

def tr1  : ProgramGraph Nat (ActionLabel String) StateVars := compileSequence somSeq1
def tr2t : ProgramGraph Nat (ActionLabel String) StateVars := compileSequence (somSeq2 true)
def tr2f : ProgramGraph Nat (ActionLabel String) StateVars := compileSequence (somSeq2 false)
def tr3  : ProgramGraph Nat (ActionLabel String) StateVars := compileSequence somSeq3
def tr4  : ProgramGraph Nat (ActionLabel String) StateVars := compileSequence somSeq4


--#eval vizProgramGraph tr2
--#eval graphVizFSM <| compactFSM <| onlyReachableFSM <| unfoldProgramGraph (interleaveProgramGraphs tr1 tr3) readableNodeLabel id

--#eval findProgramStates (interleaveProgramGraphs tr1 tr3 .none) (hasAllLabels ["done1","done3"])

def testProgramGraphBasics : TestSeq :=
    group "Program Graph Basics"
        <| test "do loop" (findProgramStates tr1 [default] (hasLabel "done1") == [{ x := true, y := 0 }])
        <| test "if then true" (findProgramStates tr2t [default] (hasLabel "done2") == [{ x := true, y := 3 }])
        <| test "if then false" (findProgramStates tr2f [default] (hasLabel "done2") == [{ x := false, y := 0 }])
        <| test "simple add" (findProgramStates tr3 [default] (hasLabel "done3") == [{ x:= false, y:= 1}])

--#lspec testProgramGraphBasics

def testInterleaving : TestSeq :=
    group "Program Graph Interleaving"
        <| test "with do loop" (findProgramStates (interleaveProgramGraphs tr1 tr3 .none) [default] (hasAllLabels ["done1","done3"]) == [{x:=true,y:=0},{x:=false,y:=0},{x:=false,y:=1}])
        <| test "with if then" (findProgramStates (interleaveProgramGraphs tr2f tr4 .none) [default] (hasAllLabels ["done2","done4"])  == [{x:=false,y:=0},{x:=true,y:=3},{x:=true,y:=0}])

--#lspec testInterleaving


--
-- test signals (synchronous interleaving) and wait
--

def signalProg1 : ProgramGraph Nat (ActionLabel String) StateVars :=
    let seq :=
            <[λs => {s with y := 1}]>
        //> SYNC "step 1" ..SYNC
        //> END "done1" ..END
    compileSequence seq

def signalProg2 : ProgramGraph Nat (ActionLabel String) StateVars :=
    let seq :=
            SYNC "step 1" ..SYNC
        //> <[λs => {s with x := true, y := 2}]>
        //> END "done2" ..END
    compileSequence seq

def signalProg3 : ProgramGraph Nat (ActionLabel String) StateVars :=
    let seq :=
            <[λs => {s with y := 1}]>
        //> WAIT λs => s.x THEN λs => {s with y := 3} ..WAIT
        //> END "done3" ..END
    compileSequence seq


def testSyncWait : TestSeq :=
    group "Program Graph Sync and Wait"
        <| test "no sync has multiple results" (findProgramStates (interleaveProgramGraphs signalProg1 signalProg2 .none) [default] (hasAllLabels ["done1","done2"]) == [{x:=true,y:=1},{x:=true,y:=2}])
        <| test "with sync has single result " (findProgramStates (interleaveProgramGraphs signalProg1 signalProg2 (.some isSignal)) [default] (hasAllLabels ["done1","done2"])  == [{x:=true,y:=2}])
        <| test "this wait never completes "   (findProgramStates (interleaveProgramGraphs signalProg1 signalProg3 .none) [default] (hasAllLabels ["done1","done3"])  == [])
        <| test "this wait gives one result "  (findProgramStates (interleaveProgramGraphs signalProg2 signalProg3 .none) [default] (hasAllLabels ["done2","done3"])  == [{x:=true,y:=3}])

--#lspec testSyncWait


def main :=
    lspecIO
        <| .append testProgramGraphBasics
        <| .append testInterleaving
        <|         testSyncWait

