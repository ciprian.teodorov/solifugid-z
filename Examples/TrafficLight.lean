--
-- Example program for a simple traffic light, useful for testing/educational purposes
--

import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM

open APBits LabeledGraph FSM

def TrafficAPList := ["red","green","yellow"]


-- A simple green/red light. Starts in the red state. No synchronization.
def trafficLightRedGreenStrings : FSM Nat (List String) Unit :=
    {
      toLabeledGraph := fromExplicit <|
         compileGraph
             [⟨0,["red"]⟩, ⟨1,["green"]⟩]
             [⟨0,1,()⟩, ⟨1,0,()⟩]
    
      startStates := [0],
      endStates := []
    }

def trafficLightRedGreen : FSM Nat (APBits TrafficAPList) Unit :=
    FSM.mapLabels trafficLightRedGreenStrings listToAPBits

-- A simple green/yellow/red light.
def trafficLightRedGreenYellowStrings : FSM Nat (List String) Unit :=
    {
      toLabeledGraph := fromExplicit <|
         compileGraph
             [⟨0,["red"]⟩, ⟨1,["green"]⟩, ⟨2,["yellow"]⟩]
             [⟨0,1,()⟩, ⟨1,2,()⟩, ⟨2,0,()⟩]
    
      startStates := [0],
      endStates := []
    }

def trafficLightRedGreenYellow : FSM Nat (APBits TrafficAPList) Unit :=
    FSM.mapLabels trafficLightRedGreenYellowStrings listToAPBits
    
-- Model of a looping green/yellow/red light where any state can loop back on itself an infinite amount of times.
-- This allows for 'stuck' states where (for example) the system just loops infinitely on green.
def trafficLightLoopingStrings : FSM Nat (List String) Unit :=
    {
      toLabeledGraph := fromExplicit <|
         compileGraph
             [⟨0,["red"]⟩, ⟨1,["green"]⟩, ⟨2,["yellow"]⟩]
             [⟨0,1,()⟩, ⟨1,2,()⟩, ⟨2,0,()⟩,
              ⟨0,0,()⟩, ⟨1,1,()⟩, ⟨2,2,()⟩]
    
      startStates := [0],
      endStates := []
    }

def trafficLightLooping : FSM Nat (APBits TrafficAPList) Unit :=
    FSM.mapLabels trafficLightLoopingStrings listToAPBits

-- traffic light with multiple yellow and green nodes. Used mainly for testing bisimulation quotienting.
--
--      /-> [3yellow] -> [1green]  ------------> loop back to red
--     /                     ^
-- [red0]                    |
--     \                     |
--      \-> [4yellow] -> [5yellow] -> [2green] -> loop back to red
--
def trafficLightMultiStrings : FSM Nat (List String) Unit :=
    {
      toLabeledGraph := fromExplicit <|
         compileGraph
             [⟨0,["red"]⟩, ⟨1,["green"]⟩, ⟨2,["green"]⟩, ⟨3,["yellow"]⟩, ⟨4,["yellow"]⟩, ⟨5,["yellow"]⟩]
             [⟨0,3,()⟩, ⟨0,4,()⟩, ⟨3,1,()⟩, ⟨1,0,()⟩, ⟨4,5,()⟩, ⟨5,2,()⟩, ⟨2,0,()⟩, ⟨5,1,()⟩]
    
      startStates := [0],
      endStates := []
    }

def trafficLightMulti : FSM Nat (APBits TrafficAPList) Unit :=
    FSM.mapLabels trafficLightMultiStrings listToAPBits


-- traffic light with multiple yellow and green nodes, and a cycle of yellow nodes with loops.
-- Used mainly for testing stutter-bisimulation quotienting.
--
--      /-> [3yellow] -> [1green] --> [6green] ----------> loop back to red
--     /        |  ^          ^           ^
-- [red0]       |  \-------\  |           |
--     \        v          |  |           |
--      \-> [4yellow] -> [5yellow] -> [2green] -> loop back to red
--
def trafficLightCycleStrings : FSM Nat (List String) Unit :=
    {
      toLabeledGraph := fromExplicit <|
         compileGraph
             [⟨0,["red"]⟩, ⟨1,["green"]⟩, ⟨2,["green"]⟩, ⟨3,["yellow"]⟩, ⟨4,["yellow"]⟩, ⟨5,["yellow"]⟩, ⟨6,["green"]⟩]
             [⟨0,3,()⟩, ⟨0,4,()⟩,  -- red to yellow
              ⟨3,4,()⟩, ⟨4,5,()⟩, ⟨5,3,()⟩, -- yellow cycle (3-4-5-3)
              ⟨3,1,()⟩, ⟨5,1,()⟩, ⟨5,2,()⟩, -- yellow to green
              ⟨1,6,()⟩, ⟨2,6,()⟩, -- green to greeen, non-cycle
              ⟨2,0,()⟩, ⟨6,0,()⟩           -- green to red
              ],
      startStates := [0],
      endStates := []
    }

def trafficLightCycle : FSM Nat (APBits TrafficAPList) Unit :=
    FSM.mapLabels trafficLightCycleStrings listToAPBits
