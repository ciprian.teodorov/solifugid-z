--
-- example programs for concurrent processes accessing a critical section.
--

import SolifugidZ.Basic
import SolifugidZ.APBits
import SolifugidZ.LabeledGraph
import SolifugidZ.FSM
import SolifugidZ.ProgramGraph

open Basic APBits LabeledGraph ProgramGraph

-- This version synchronizes critical section access via labeled edges. Once
-- all the ProgramGraphs are interleaved together the resulting transition system needs to
-- be combined with a transition system representing the mutex. The combining should be done
-- use syncProduct so that edge labels are performed synchronously.
def syncMutexProcess (n : Nat) : FSM Nat (List String) (ActionLabel String) :=
    let pId := toString n
    {
      toLabeledGraph := fromExplicit <|
         compileGraph
             [⟨0,["noncrit" ++ pId]⟩, ⟨1,["wait" ++ pId]⟩, ⟨2,["crit" ++ pId]⟩]
             [⟨0,1,.InfoText <| "acquire" ++ pId⟩, ⟨1,2,.Signal "lock"⟩, ⟨2,0,.Signal "unlock"⟩]
    
      startStates := [0],
      endStates := []
    }

-- transition system of a mutex, to be used with system(s) created via syncMutexProcess
def mutexSystem : FSM Nat (List String) (ActionLabel String) :=
    {
      toLabeledGraph := fromExplicit <|
         compileGraph
             [⟨0,["unlocked"]⟩, ⟨1,["locked"]⟩]
             [⟨0,1,.Signal "lock"⟩, ⟨1,0,.Signal "unlock"⟩]
    
      startStates := [0],
      endStates := []
    }

-- The various atomic propositions that appear in this system
def CriticalSectionAPList := ["wait1","wait2","crit1","crit2","noncrit1","noncrit2","locked","unlocked"]

-- builds up an entire transition system representing two processes accessing a critical section
def criticalSectionSystem : FSM Nat (APBits CriticalSectionAPList) (ActionLabel String) :=
    -- first build and combine (interleave) the two programs accessing the critical section
    let process1 := syncMutexProcess 1
    let process2 := syncMutexProcess 2
    let processes := FSM.compactFSM <| FSM.productFSMs process1 process2
    -- next we combine the processes with the mutex they will use to synchronize access
    -- to the critical section. Here we use syncProductFSMs which means any edges
    -- that are .Signal edges (determined by isSignal) cannot be used unless both
    -- the program and the mutex system transition at the same time.
    let combinedSystem := FSM.compactFSM <| FSM.onlyReachableFSM <| FSM.syncProductFSMs processes mutexSystem isSignal 
    FSM.mapLabels combinedSystem listToAPBits

--#eval FSM.graphVizFSM criticalSectionSystem
