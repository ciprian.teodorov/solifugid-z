import Lake
open System Lake DSL

package «solifugid-z»

require LSpec from git "https://github.com/yatima-inc/LSpec.git"@"88f7d23e56a061d32c7173cea5befa4b2c248b41"

lean_lib SolifugidZ {
}

lean_lib Examples {
}

lean_lib VizGraph {

}


--
-- This chunk of stuff builds and packages the javascript module used by VizWidget.
-- Taken from the RubiksCube example in lean4-samples.
--
-- "lake build vizWidget"
--
-- to run a watcher for auto-rebuild when vizWidget.tsx is saved, use
-- "npm run watch -- --tsxName vizWidget" in the /widget directory.
--

def npmCmd : String :=
  if Platform.isWindows then "npm.cmd" else "npm"

target packageLock : FilePath := do
  let widgetDir := __dir__ / "SolifugidZ" / "widget"
  let packageFile ← inputFile <| widgetDir / "package.json"
  let packageLockFile := widgetDir / "package-lock.json"
  buildFileAfterDep packageLockFile packageFile fun _srcFile => do
    proc {
      cmd := npmCmd
      args := #["install"]
      cwd := some widgetDir
    }

def tsxTarget (pkg : Package) (tsxName : String) [Fact (pkg.name = _package.name)]
    : IndexBuildM (BuildJob FilePath) := do
  let widgetDir := __dir__ / "SolifugidZ" / "widget"
  let jsFile := widgetDir / "dist" / s!"{tsxName}.js"
  let deps : Array (BuildJob FilePath) := #[
    ← inputFile <| widgetDir / "src" / s!"{tsxName}.tsx",
    ← inputFile <| widgetDir / "rollup.config.js",
    ← inputFile <| widgetDir / "tsconfig.json",
    ← fetch (pkg.target ``packageLock)
  ]
  buildFileAfterDepArray jsFile deps fun _srcFile => do
    proc {
      cmd := npmCmd
      args := #["run", "build", "--", "--tsxName", tsxName]
      cwd := some widgetDir
    }

target vizWidget (pkg : Package) : FilePath := tsxTarget pkg "vizWidget"



--
-- to run tests use "lake exe lspec"
--
-- tests for LabeledGraph and FSM
lean_exe Tests.FSM
lean_exe Tests.GraphUtilities

-- tests for NFA/NBA
lean_exe Tests.NFA
lean_exe Tests.NBA
lean_exe Tests.ProgramGraph

--test for LTL/CTL
lean_exe Tests.LTL
lean_exe Tests.CTL
lean_exe Tests.CTLFair
lean_exe Tests.Bisimulation
lean_exe Tests.StutterBisimulation
